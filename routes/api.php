<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('items')->group(function () {
    Route::get('creation', 'Api\ItemsController@creation')->name('api.items.creation');
    Route::get('edition/{campaign}', 'Api\ItemsController@edition')->name('api.items.edition');
});