<?php

// auth
Auth::routes();

// home
Route::get('/', 'HomeController@index')->name('home');

// campaign items
Route::get('items/{item}', 'ItemController@show')->name('items.show');
Route::get('items/{item}/image/download', 'ItemController@download')->name('items.download');

// User profiles
Route::get('profiles/{user}', 'ProfileController@show')->name('profiles.show');

// dashboard
Route::prefix('admin')->group(function () {
    Route::get('/', 'Admin\DashboardController@index')->name('dashboard');
    
    // users
    Route::prefix('users')->group(function () {
        Route::get('/', 'Admin\UserController@index')->name('admin.users.index');
        Route::get('add', 'Admin\UserController@showRegistrationForm')->name('admin.users.create');
        Route::post('/', 'Admin\UserController@register')->name('admin.users.store');
        Route::get('{user}/edit', 'Admin\UserController@edit')->name('admin.users.edit');
        Route::patch('{user}', 'Admin\UserController@update')->name('admin.users.update');
        Route::get('{user}/delete', 'Admin\UserController@delete')->name('admin.users.delete');
        Route::delete('{user}', 'Admin\UserController@destroy')->name('admin.users.destroy');
    });
    
    // campaigns
    Route::prefix('campaigns')->group(function () {
        Route::get('/', 'Admin\CampaignController@index')->name('admin.campaigns.index');
        Route::get('add', 'Admin\CampaignController@create')->name('admin.campaigns.create');
        Route::post('/', 'Admin\CampaignController@store')->name('admin.campaigns.store');
        Route::get('{campaign}/edit', 'Admin\CampaignController@edit')->name('admin.campaigns.edit');
        Route::patch('{campaign}', 'Admin\CampaignController@update')->name('admin.campaigns.update');
        Route::get('{campaign}/delete', 'Admin\CampaignController@delete')->name('admin.campaigns.delete');
        Route::delete('{campaign}', 'Admin\CampaignController@destroy')->name('admin.campaigns.destroy');
    });
    
    // categories
    Route::prefix('categories')->group(function () {
        Route::get('/', 'Admin\CategoryController@index')->name('admin.categories.index');
        Route::get('add', 'Admin\CategoryController@create')->name('admin.categories.create');
        Route::post('/', 'Admin\CategoryController@store')->name('admin.categories.store');
        Route::get('{category}/edit', 'Admin\CategoryController@edit')->name('admin.categories.edit');
        Route::patch('{category}', 'Admin\CategoryController@update')->name('admin.categories.update');
        Route::get('{category}/delete', 'Admin\CategoryController@delete')->name('admin.categories.delete');
        Route::delete('{category}', 'Admin\CategoryController@destroy')->name('admin.categories.destroy');
    });
    
    // tags
    Route::prefix('tags')->group(function () {
        Route::get('/', 'Admin\TagController@index')->name('admin.tags.index');
        Route::get('add', 'Admin\TagController@create')->name('admin.tags.create');
        Route::post('/', 'Admin\TagController@store')->name('admin.tags.store');
        Route::get('{tag}/edit', 'Admin\TagController@edit')->name('admin.tags.edit');
        Route::patch('{tag}', 'Admin\TagController@update')->name('admin.tags.update');
        Route::get('{tag}/delete', 'Admin\TagController@delete')->name('admin.tags.delete');
        Route::delete('{tag}', 'Admin\TagController@destroy')->name('admin.tags.destroy');
    });
    
    // campaign items
    Route::prefix('items')->group(function () {
        Route::get('/', 'Admin\ItemController@index')->name('admin.items.index');
        Route::get('add', 'Admin\ItemController@create')->name('admin.items.create');
        Route::post('/', 'Admin\ItemController@store')->name('admin.items.store');
        Route::get('{item}/edit', 'Admin\ItemController@edit')->name('admin.items.edit');
        Route::patch('{item}', 'Admin\ItemController@update')->name('admin.items.update');
        Route::get('{item}/delete', 'Admin\ItemController@delete')->name('admin.items.delete');
        Route::delete('{item}', 'Admin\ItemController@destroy')->name('admin.items.destroy');
    });
});