<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminCategoryTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function a_non_admin_user_cannot_create_a_category()
    {
        $this->withExceptionHandling()->signIn();
        
        $this->post(route('admin.categories.store'), ['name' => 'New category'])
            ->assertStatus(403);
        
        $this->assertDatabaseMissing('categories', ['name' => 'New category']);
    }
    
    /** @test */
    public function an_admin_user_can_create_a_category()
    {
        $this->createCategory(['name' => 'A new category name']);
        
        $this->assertDatabaseHas('categories', ['name' => 'A new category name']);
    }
    
    /** @test */
    public function a_category_requires_a_name()
    {
        $this->createCategory(['name' => null])
            ->assertSessionHasErrors(['name']);
    }
    
    /** @test */
    public function an_admin_user_can_update_a_category()
    {
        $this->createCategory(['name' => 'A category name']);
        
        $this->patch(route('admin.categories.update', \App\Category::first()), [
            'name' => 'New category name'
        ]);
        
        $this->assertDatabaseHas('categories', ['name' => 'New category name']);
    }
    
    /** @test */
    public function an_admin_user_can_delete_a_category()
    {
        $this->createCategory(['name' => 'A category name']);
        
        $this->delete(route('admin.categories.destroy', \App\Category::first()));
        
        $this->assertDatabaseMissing('categories', ['id' => 1]);
    }
    
    /**
     * Create a category as an admin user
     */
    public function createCategory(array $data)
    {
        $this->withExceptionHandling()->signInAsAdmin();
        
        return $this->post(route('admin.categories.store'), $data);
    }
}
