<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProfileTest extends TestCase
{
    use RefreshDatabase;
    
    protected $user;
    
    public function setUp()
    {
        parent::setUp();
        
        $this->user = create('App\User');
    }
    
    /** @test */
    public function a_user_has_a_profile()
    {
        $this->get(route('profiles.show', $this->user))
            ->assertViewIs('profiles.show')
            ->assertSee("<h1>{$this->user->name}</h1>");
    }
}
