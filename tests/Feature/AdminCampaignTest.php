<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminCampaignTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function only_an_admin_user_can_access_the_admin_campaigns_page()
    {
        $this->withExceptionHandling();
        
        $this->get(route('admin.campaigns.index'))
            ->assertStatus(403);
    
        $this->signIn();
    
        $this->get(route('admin.campaigns.index'))
            ->assertStatus(403);
        
        $this->signInAsAdmin();
    
        $this->get(route('admin.campaigns.index'))
            ->assertSee('<h1>'.__('interface.admin.campaign.index').'</h1>');
    }
    
    /** @test */
    public function a_non_admin_user_cannot_create_a_campaign()
    {
        $this->withExceptionHandling();
        
        $this->post(route('admin.campaigns.store'), ['title' => 'New campaign', 'published' => true])
            ->assertStatus(403);
        
        $this->assertDatabaseMissing('campaigns', ['title' => 'New campaign']);
    
        $this->signIn();
    
        $this->post(route('admin.campaigns.store'), ['title' => 'New campaign', 'published' => true])
            ->assertStatus(403);
    
        $this->assertDatabaseMissing('items', ['title' => 'New item']);
    }
    
    /** @test */
    public function an_admin_user_can_create_a_campaign()
    {
        $this->createCampaign(['title' => 'A new campaign', 'published' => true]);
        
        $this->assertDatabaseHas('campaigns', ['title' => 'A new campaign']);
    }
    
    /** @test */
    public function a_campaign_requires_title_and_published()
    {
        $this->createCampaign(['title' => null, 'published' => null])
            ->assertSessionHasErrors(['title', 'published']);
    }
    
    /** @test */
    public function the_categories_are_saved()
    {
        $this->createCategories();
        
        $this->createCampaign(['title' => 'A new campaign', 'category_id' => [1, 2], 'published' => true]);
        
        $this->assertDatabaseHas('campaign_category', ['campaign_id' => 1, 'category_id' => 1]);
        $this->assertDatabaseHas('campaign_category', ['campaign_id' => 1, 'category_id' => 2]);
    }
    
    /** @test */
    public function the_districts_are_saved()
    {
        $this->createDistricts();
        
        $this->createCampaign(['title' => 'A new campaign', 'district_id' => [1, 3], 'published' => true]);
        
        $this->assertDatabaseHas('campaign_district', ['campaign_id' => 1, 'district_id' => 1]);
        $this->assertDatabaseHas('campaign_district', ['campaign_id' => 1, 'district_id' => 3]);
    }
    
    /** @test */
    public function the_tags_are_saved()
    {
        $this->createTags();
        
        $this->createCampaign(['title' => 'A new campaign', 'tag_id' => [1, 2], 'published' => true]);
        
        $this->assertDatabaseHas('campaign_tag', ['campaign_id' => 1, 'tag_id' => 1]);
        $this->assertDatabaseHas('campaign_tag', ['campaign_id' => 1, 'tag_id' => 2]);
        
        $this->createCampaign(['title' => 'Another campaign', 'tag_id' => [2, 3], 'published' => true]);
        
        $this->assertDatabaseHas('campaign_tag', ['campaign_id' => 2, 'tag_id' => 2]);
        $this->assertDatabaseHas('campaign_tag', ['campaign_id' => 2, 'tag_id' => 3]);
    }
    
    /** @test */
    public function the_related_items_are_saved()
    {
        $this->createItemTypes();
        
        $this->createItems();
    
        $this->createCampaign(['title' => 'A new campaign', 'items' => [1, 2], 'published' => true]);
    
        $this->assertDatabaseHas('items', ['id' => 1, 'campaign_id' => 1]);
        $this->assertDatabaseHas('items', ['id' => 2, 'campaign_id' => 1]);
    }
    
    /** @test */
    public function the_date_must_be_in_year_month_day_format()
    {
        $this->createCampaign(['title' => 'A new campaign', 'date' => '23-12-2017', 'published' => true])
            ->assertSessionHasErrors(['date']);
    
        $this->assertDatabaseMissing('campaigns', ['title' => 'New campaign']);
    
        $this->createCampaign(['title' => 'A new campaign', 'date' => '2017-12-23', 'published' => true]);
    
        $this->assertDatabaseHas('campaigns', ['title' => 'A new campaign']);
    }
    
    /** @test */
    public function a_non_admin_user_cannot_update_a_campaign()
    {
        $this->createCampaign(['title' => 'A new campaign', 'published' => true]);
        
        $campaign = \App\Campaign::first();
        
        $this->logout();
        
        $this->patch(route('admin.campaigns.update', $campaign), ['title' => 'Some new title'])
            ->assertStatus(403);
    
        $this->signIn();
    
        $this->patch(route('admin.campaigns.update', $campaign), ['title' => 'Some new title'])
            ->assertStatus(403);
        
        $this->assertDatabaseHas('campaigns', ['title' => 'A new campaign']);
    }
    
    /** @test */
    public function an_admin_user_can_update_a_campaign()
    {
        $this->createCampaign(['title' => 'A new campaign', 'published' => true]);
    
        $campaign = \App\Campaign::first();
    
        $this->patch(route('admin.campaigns.update', $campaign), ['title' => 'Some new title', 'published' => $campaign->published]);
        
        $this->assertDatabaseHas('campaigns', ['title' => 'Some new title']);
    }
    
    /** @test */
    public function a_campaign_update_still_requires_title_and_published()
    {
        $this->createCampaign(['title' => 'A new campaign', 'published' => true]);
    
        $campaign = \App\Campaign::first();
        
        $this->patch(route('admin.campaigns.update', $campaign), ['title' => null, 'published' => null])
            ->assertSessionHasErrors(['title', 'published']);
    }
    
    /** @test */
    public function an_update_may_change_the_related_items()
    {
        $this->createItemTypes();
        
        $this->createItems();
        
        $this->createCampaign(['title' => 'A new campaign', 'items' => [1], 'published' => true]);
    
        $campaign = \App\Campaign::first();
    
        $this->patch(route('admin.campaigns.update', $campaign), ['title' => $campaign->title, 'items' => [2, 3], 'published' => $campaign->published]);
    
        $this->assertDatabaseMissing('items', ['id' => 1, 'campaign_id' => 1]);
        $this->assertDatabaseHas('items', ['id' => 2, 'campaign_id' => 1]);
        $this->assertDatabaseHas('items', ['id' => 3, 'campaign_id' => 1]);
    }
    
    /** @test */
    public function a_non_admin_user_cannot_delete_a_campaign()
    {
        $this->createCampaign(['title' => 'A new campaign', 'published' => true]);
    
        $campaign = \App\Campaign::first();
    
        $this->logout();
        
        $this->delete(route('admin.campaigns.destroy', $campaign))
            ->assertStatus(403);
    
        $this->signIn();
    
        $this->delete(route('admin.campaigns.destroy', $campaign))
            ->assertStatus(403);
        
        $this->assertDatabaseHas('campaigns', ['id' => $campaign->id]);
    }
    
    /** @test */
    public function an_admin_user_can_delete_a_campaign()
    {
        $this->createCampaign(['title' => 'A new campaign', 'published' => true]);
    
        $campaign = \App\Campaign::first();
        
        $this->delete(route('admin.campaigns.destroy', $campaign));
        
        $this->assertDatabaseMissing('campaigns', ['id' => $campaign->id]);
    }
    
    /** @test */
    public function on_deleting_a_campaign_the_taxonomies_related_to_it_are_removed()
    {
        $this->createCategories();
        $this->createDistricts();
        $this->createTags();
        
        $this->createCampaign(['title' => 'A new campaign', 'category_id' => [1, 2], 'district_id' => [3, 4], 'tag_id' => [1, 5], 'published' => true]);
        
        $campaign = \App\Campaign::first();
        
        $this->delete(route('admin.campaigns.destroy', $campaign));
        
        $this->assertEmpty(\App\Category::find(1)->campaigns);
        $this->assertEmpty(\App\Category::find(2)->campaigns);
        $this->assertEmpty(\App\District::find(3)->campaigns);
        $this->assertEmpty(\App\District::find(4)->campaigns);
        $this->assertEmpty(\App\Tag::find(1)->campaigns);
        $this->assertEmpty(\App\Tag::find(5)->campaigns);
    }
    
    /**
     * Create a campaign as an admin user
     */
    public function createCampaign(array $data)
    {
        $this->withExceptionHandling()->signInAsAdmin();
        
        return $this->post(route('admin.campaigns.store'), $data);
    }
}
