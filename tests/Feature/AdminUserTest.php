<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminUserTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function only_an_admin_user_can_access_the_admin_users_page()
    {
        $this->withExceptionHandling();
    
        $this->get(route('admin.users.index'))
            ->assertStatus(403);
    
        $this->signIn();
    
        $this->get(route('admin.users.index'))
            ->assertStatus(403);
    
        $this->signInAsAdmin();
    
        $this->get(route('admin.users.index'))
            ->assertSee('<h1>'.__('interface.admin.user.index').'</h1>');
    }
    
    /** @test */
    public function only_an_admin_user_can_create_a_user()
    {
        $this->withExceptionHandling();
    
        $this->post(route('admin.users.store'), ['name' => 'John Test'])
            ->assertStatus(403);
    
        $this->assertDatabaseMissing('users', ['name' => 'John Test']);
    
        $this->signIn();
    
        $this->post(route('admin.users.store'), ['name' => 'Barbara Test'])
            ->assertStatus(403);
    
        $this->assertDatabaseMissing('users', ['name' => 'Barbara Test']);
    
        $this->signInAsAdmin();
    
        $this->post(route('admin.users.store'), ['name' => 'Augustus Test', 'email' => 'somevalid@email.com', 'password' => 'secret', 'password_confirmation' => 'secret']);
    
        $this->assertDatabaseHas('users', ['name' => 'Augustus Test']);
    }
    
    /** @test */
    public function a_user_requires_name_email_and_twice_the_same_password()
    {
        $this->withExceptionHandling()->signInAsAdmin();
    
        $this->post(route('admin.users.store'), ['name' => null, 'email' => null, 'password' => null])
            ->assertSessionHasErrors(['name', 'email', 'password']);
    
        $this->post(route('admin.users.store'), ['name' => 'Auhustus test', 'email' => 'somevalid@email.com', 'password' => 'secret', 'password_confirmation' => 'different password'])
            ->assertSessionHasErrors(['password']);
    }
    
    /** @test */
    public function only_an_admin_user_can_edit_a_user()
    {
        $this->withExceptionHandling();
        
        $user = create('App\User');
    
        $newData = [
            'name' => $user->name . ' Jr.',
            'email' => 'anothervalid@email.com',
        ];
    
        $this->patch(route('admin.users.update', $user), $newData)
            ->assertStatus(403);
    
        $this->signIn();
    
        $this->patch(route('admin.users.update', $user), $newData)
            ->assertStatus(403);
    
        $this->signInAsAdmin();
        
        $this->patch(route('admin.users.update', $user), $newData);
        
        $this->assertDatabaseHas('users', $newData);
    }
    
    /** @test */
    public function a_user_update_still_requires_name_and_email()
    {
        $this->withExceptionHandling()->signInAsAdmin();
        
        $user = create('App\User');
        
        $newData = [
            'name' => null,
            'email' => 'notvalidemail',
        ];
        
        $this->patch(route('admin.users.update', $user), $newData)
            ->assertSessionHasErrors(['name', 'email']);
    }
    
    /** @test */
    public function only_an_admin_user_can_delete_a_user()
    {
        $this->withExceptionHandling();
    
        $user = create('App\User');
    
        $this->delete(route('admin.users.destroy', $user))
            ->assertStatus(403);
    
        $this->signIn();
    
        $this->delete(route('admin.users.destroy', $user))
            ->assertStatus(403);
        
        $this->signInAsAdmin();
        
        $this->delete(route('admin.users.destroy', $user));
        
        $this->assertDatabaseMissing('users', ['id' => $user->id]);
    }
    
    /** @test */
    public function an_admin_user_cannot_delete_herself()
    {
        $this->withExceptionHandling()->signInAsAdmin();
        
        $this->delete(route('admin.users.destroy', auth()->user()));
        
        $this->assertDatabaseHas('users', ['id' => auth()->id()]);
    }
}
