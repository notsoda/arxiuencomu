<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminTagTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function a_non_admin_user_cannot_create_a_tag()
    {
        $this->withExceptionHandling()->signIn();
        
        $this->post(route('admin.tags.store'), ['name' => 'New tag'])
            ->assertStatus(403);
        
        $this->assertDatabaseMissing('tags', ['name' => 'New tag']);
    }
    
    /** @test */
    public function an_admin_user_can_create_a_tag()
    {
        $this->createTag(['name' => 'A new tag name']);
        
        $this->assertDatabaseHas('tags', ['name' => 'A new tag name']);
    }
    
    /** @test */
    public function a_tag_requires_a_name()
    {
        $this->createTag(['name' => null])
            ->assertSessionHasErrors(['name']);
    }
    
    /** @test */
    public function an_admin_user_can_update_a_tag()
    {
        $this->createTag(['name' => 'A tag name']);
        
        $this->patch(route('admin.tags.update', \App\Tag::first()), [
            'name' => 'New tag name'
        ]);
        
        $this->assertDatabaseHas('tags', ['name' => 'New tag name']);
    }
    
    /** @test */
    public function an_admin_user_can_delete_a_tag()
    {
        $this->createTag(['name' => 'A tag name']);
        
        $this->delete(route('admin.tags.destroy', \App\Tag::first()));
        
        $this->assertDatabaseMissing('tags', ['id' => 1]);
    }
    
    /**
     * Create a tag as an admin user
     */
    public function createTag(array $data)
    {
        $this->withExceptionHandling()->signInAsAdmin();
        
        return $this->post(route('admin.tags.store'), $data);
    }
}
