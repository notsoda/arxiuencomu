<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminItemTest extends TestCase
{
    use RefreshDatabase;
    
    public function setUp()
    {
        parent::setUp();
    
        $this->createItemTypes();
    }
    
    /** @test */
    public function only_an_admin_user_can_access_the_admin_items_page()
    {
        $this->withExceptionHandling();
        
        $this->get(route('admin.items.index'))
            ->assertStatus(403);
    
        $this->signIn();
    
        $this->get(route('admin.items.index'))
            ->assertStatus(403);
        
        $this->signInAsAdmin();
    
        $this->get(route('admin.items.index'))
            ->assertSee('<h1>'.__('interface.admin.item.index').'</h1>');
    }
    
    /** @test */
    public function a_non_admin_user_cannot_create_an_item()
    {
        $this->withExceptionHandling();
        
        $this->post(route('admin.items.store'), ['title' => 'New item', 'item_type_id' => 1, 'published' => true])
            ->assertStatus(403);
        
        $this->assertDatabaseMissing('items', ['title' => 'New item']);
    
        $this->signIn();
    
        $this->post(route('admin.items.store'), ['title' => 'New item', 'item_type_id' => 1, 'published' => true])
            ->assertStatus(403);
    
        $this->assertDatabaseMissing('items', ['title' => 'New item']);
    }
    
    /** @test */
    public function an_admin_user_can_create_an_item()
    {
        $this->createItem(['title' => 'A new item', 'item_type_id' => 1, 'published' => true]);
        
        $this->assertDatabaseHas('items', ['title' => 'A new item', 'item_type_id' => 1]);
    }
    
    /** @test */
    public function an_item_requires_title_type_and_published()
    {
        $this->createItem(['title' => null, 'item_type_id' => null, 'published' => null])
            ->assertSessionHasErrors(['title', 'item_type_id', 'published']);
    }
    
    /** @test */
    public function an_item_requires_a_valid_type()
    {
        $this->createItem(['title' => 'A new item', 'item_type_id' => 6, 'published' => true])
            ->assertSessionHasErrors(['item_type_id']);
    }
    
    /** @test */
    public function a_non_admin_user_cannot_update_an_item()
    {
        $this->createItem(['title' => 'A new item', 'item_type_id' => 1, 'published' => true]);
        
        $item = \App\Item::first();
        
        $this->logout();
        
        $this->patch(route('admin.items.update', $item), ['title' => 'Some new title'])
            ->assertStatus(403);
    
        $this->signIn();
    
        $this->patch(route('admin.items.update', $item), ['title' => 'Some new title'])
            ->assertStatus(403);
        
        $this->assertDatabaseHas('items', ['title' => 'A new item']);
    }
    
    /** @test */
    public function an_admin_user_can_update_an_item()
    {
        $this->createItem(['title' => 'A new item', 'item_type_id' => 1, 'published' => true]);
    
        $item = \App\Item::first();
    
        $this->patch(route('admin.items.update', $item), ['title' => 'Some new title', 'item_type_id' => $item->item_type_id, 'published' => $item->published]);
        
        $this->assertDatabaseHas('items', ['title' => 'Some new title']);
    
        $this->patch(route('admin.items.update', $item), ['title' => $item->title, 'item_type_id' => 2, 'published' => $item->published]);
    
        $this->assertDatabaseHas('items', ['item_type_id' => 2]);
    }
    
    /** @test */
    public function a_non_admin_user_cannot_delete_an_item()
    {
        $this->createItem(['title' => 'A new item', 'item_type_id' => 1, 'published' => true]);
    
        $item = \App\Item::first();
    
        $this->logout();
        
        $this->delete(route('admin.items.destroy', $item))
            ->assertStatus(403);
    
        $this->signIn();
    
        $this->delete(route('admin.items.destroy', $item))
            ->assertStatus(403);
        
        $this->assertDatabaseHas('items', ['id' => $item->id]);
    }
    
    /** @test */
    public function an_admin_user_can_delete_an_item()
    {
        $this->createItem(['title' => 'A new item', 'item_type_id' => 1, 'published' => true]);
    
        $item = \App\Item::first();
        
        $this->delete(route('admin.items.destroy', $item));
        
        $this->assertDatabaseMissing('items', ['id' => $item->id]);
    }
    
    /**
     * Create an item as an admin user
     */
    public function createItem(array $data)
    {
        $this->withExceptionHandling()->signInAsAdmin();
        
        return $this->post(route('admin.items.store'), $data);
    }
}
