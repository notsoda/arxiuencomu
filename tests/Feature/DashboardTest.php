<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DashboardTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function only_an_admin_user_can_access_the_dashboard()
    {
        $this->withExceptionHandling();
        
        $this->get(route('dashboard'))
            ->assertStatus(403);
        
        $this->signIn();
        
        $this->get(route('dashboard'))
            ->assertStatus(403);
    
        $this->signInAsAdmin();
    
        $this->get(route('dashboard'))
            ->assertSee('<h1>'.__('interface.admin.dashboard.title').'</h1>');
    }
}
