<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Merujan99\LaravelVideoEmbed\Facades\LaravelVideoEmbed;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeTest extends TestCase
{
    use RefreshDatabase;
    
    public function setUp()
    {
        parent::setUp();
    
        $this->createItemTypes();
    
        Storage::fake('public');
    }
    
    /** @test */
    public function it_shows_all_published_campaign_details()
    {
        $this->createSomeCampaigns();
        
        $this->get(route('home'))
            ->assertSee(\App\Campaign::find(1)->title)
            ->assertSee(\App\Campaign::find(2)->title)
            ->assertDontSee(\App\Campaign::find(3)->title);
    }
    
    /** @test */
    public function each_campaign_detail_shows_title_and_body()
    {
        $this->createSomeCampaigns();
        
        $this->get(route('home'))
            ->assertSee(\App\Campaign::find(1)->title)
            ->assertSee(\App\Campaign::find(1)->body)
            ->assertSee(\App\Campaign::find(2)->title)
            ->assertSee(\App\Campaign::find(2)->body);
    }
    
    /** @test */
    public function the_campaign_detail_shows_its_date_in_proper_format_and_language()
    {
        $this->createCampaign(['title' => 'First title', 'body' => 'Some first body.', 'date' => '2017-12-23', 'published' => true]);
        
        $humanAndCatalanDate = 'desembre 2017';
    
        $this->get(route('home'))
            ->assertSee($humanAndCatalanDate);
    }
    
    /** @test */
    public function the_campaign_detail_shows_the_categories_it_belongs_to()
    {
        $this->createCategories();
        
        $this->createCampaign(['title' => 'First title', 'body' => 'Some first body.', 'category_id' => [1, 2], 'published' => true]);
        
        $this->get(route('home'))
            ->assertSee(\App\Category::find(1)->name)
            ->assertSee(\App\Category::find(2)->name);
    }
    
    /** @test */
    public function the_campaign_detail_shows_the_districts_it_belongs_to()
    {
        $this->createDistricts();
        
        $this->createCampaign(['title' => 'First title', 'body' => 'Some first body.', 'district_id' => [1, 3], 'published' => true]);
        
        $this->get(route('home'))
            ->assertSee(\App\District::find(1)->name)
            ->assertSee(\App\District::find(3)->name);
    }
    
    /** @test */
    public function the_campaign_detail_shows_its_related_image_item_as_image()
    {
        $imageFile = UploadedFile::fake()->image('niceimage.jpg', 600, 600);
    
        $this->createItem(['title' => 'A new image item', 'item_type_id' => 1, 'featured_image' => $imageFile, 'published' => true]);
    
        $this->createCampaign(['title' => 'First title', 'body' => 'Some first body.', 'items' => [1], 'published' => true]);
        
        $imageUrl = \App\Item::first()->getFirstMediaUrl('featured', 'item-teaser');
        
        $this->assertNotEmpty($imageUrl);
        
        $this->get(route('home'))
            ->assertSee($imageUrl);
    }
    
    /** @test */
    public function the_campaign_detail_shows_its_related_url_item_as_title_source_and_summary()
    {
        $webData = [
            'url' => 'https://www.eldiario.es/economia/Conflicto-Edificio-Espana-capitulo-historia_0_805969923.html',
            'source' => 'eldiario.es',
            'description' => 'Some nice description',
        ];
        
        $this->createItem(['title' => 'A new news item', 'item_type_id' => 3, 'url' => $webData['url'], 'source' => $webData['source'], 'body' => $webData['description'], 'published' => true]);
        
        $this->createCampaign(['title' => 'First title', 'body' => 'Some first body.', 'items' => [1], 'published' => true]);
        
        $this->get(route('home'))
            ->assertSee('<p class="source">'.$webData['source'].'</p>')
            ->assertSee('<p>'.$webData['description']);
    }
    
    /** @test */
    public function the_url_item_may_show_an_image_which_is_saved_after_getting_it_from_the_urls_meta_og_image_tag()
    {
        $this->createItemTypes();
        
        $this->createItem(['title' => 'A new news item', 'item_type_id' => 3, 'url' => 'https://www.eldiario.es/economia/Conflicto-Edificio-Espana-capitulo-historia_0_805969923.html', 'published' => true]);
        
        $this->createCampaign(['title' => 'First title', 'body' => 'Some first body.', 'items' => [1], 'published' => true]);
        
        $externalImageUrl = \App\Item::first()->getFirstMediaUrl('external', 'item-url-teaser');
        
        $this->assertNotEmpty($externalImageUrl);
        
        $this->get(route('home'))
            ->assertSee('<img src="'.$externalImageUrl.'"');
    }
    
    /** @test */
    public function the_video_item_may_show_an_image_which_is_saved_after_getting_it_from_the_youtube_thumbnail()
    {
        $this->createItem(['title' => 'A new video item', 'item_type_id' => 2, 'video_url' => 'https://www.youtube.com/watch?v=VYOjWnS4cMY', 'published' => true]);
    
        $this->createCampaign(['title' => 'First title', 'body' => 'Some first body.', 'items' => [1], 'published' => true]);
    
        $youtubeImageUrl = \App\Item::first()->getFirstMediaUrl('youtube');
        
        $this->assertNotEmpty($youtubeImageUrl);
        
        $this->get(route('home'))
            ->assertSee($youtubeImageUrl);
    }
    
    /** @test */
    public function the_campaign_detail_shows_a_message_if_the_video_url_is_wrong()
    {
        $this->createItem(['title' => 'A new video item', 'item_type_id' => 2, 'video_url' => 'some-wrong-url', 'published' => true]);
    
        $this->createCampaign(['title' => 'First title', 'body' => 'Some first body.', 'items' => [1], 'published' => true]);
        
        $this->get(route('home'))
            ->assertSee(__('interface.video_url_wrong'));
    }
    
    /** @test */
    public function the_campaign_detail_item_links_to_an_item_detail()
    {
        $this->createItems();
        
        $this->createCampaign(['title' => 'First title', 'body' => 'Some first body.', 'items' => [1], 'published' => true]);
        
        $itemLink = '<a href="'.route('items.show', \App\Item::first());
    
        $this->get(route('home'))
            ->assertSee($itemLink);
    }
    
    /** @test */
    public function the_campaign_detail_url_item_links_to_an_external_url()
    {
        $externalUrl = 'https://www.eldiario.es/economia/Conflicto-Edificio-Espana-capitulo-historia_0_805969923.html';
        
        $this->createItem(['title' => 'A new news item', 'item_type_id' => 3, 'url' => $externalUrl, 'published' => true]);
    
        $this->createCampaign(['title' => 'First title', 'body' => 'Some first body.', 'items' => [1], 'published' => true]);
    
        $itemLink = '<a href="'.route('items.show', \App\Item::first());
        $externalLink = '<a href="'.$externalUrl.'" target="_blank"';
        
        $this->get(route('home'))
            ->assertDontSee($itemLink)
            ->assertSee($externalLink);
    }
    
    /**
     * Create an item as an admin user
     */
    public function createItem(array $data)
    {
        $this->withExceptionHandling()->signInAsAdmin();
        
        return $this->post(route('admin.items.store'), $data);
    }
    
    /**
     * Create a campaign as an admin user
     */
    public function createCampaign(array $data)
    {
        $this->withExceptionHandling()->signInAsAdmin();
        
        return $this->post(route('admin.campaigns.store'), $data);
    }
    
    public function createSomeCampaigns()
    {
        // create two normal campaigns
        $this->createCampaign(['title' => 'First title', 'body' => 'Some first body.', 'date' => '2017-12-23', 'published' => true]);
        $this->createCampaign(['title' => 'Second title', 'body' => 'Some second body.', 'date' => '2017-12-24', 'published' => true]);
        // and an unpublished one
        $this->createCampaign(['title' => 'Unpublished title', 'body' => 'Some unpublished body.', 'date' => '2017-12-25', 'published' => false]);
    }
}
