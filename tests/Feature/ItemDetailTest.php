<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ItemDetailTest extends TestCase
{
    use RefreshDatabase;
    
    public function setUp()
    {
        parent::setUp();
        
        $this->createItemTypes();
    
        $this->createCampaignsWithRelatedItems();
        
        Storage::fake('public');
    }
    
    /** @test */
    public function it_shows_the_full_image()
    {
        $item = \App\Item::first();
        
        $this->get(route('items.show', $item))
            ->assertSee($item->getFirstMediaUrl('featured'));
    }
    
    /** @test */
    public function an_item_detail_shows_pagination_links_to_previous_and_next_items_in_the_items_campaign()
    {
        $firstItem = \App\Item::find(1);
        $secondItem = \App\Item::find(2);
        $thirdItem = \App\Item::find(3);
        
        $previousLink = '<a href="'.route('items.show', $firstItem).'" class="btn-pager-prev';
        $nextLink = '<a href="'.route('items.show', $thirdItem).'" class="btn-pager-next';
        
        $this->get(route('items.show', $secondItem))
            ->assertSee($previousLink)
            ->assertSee($nextLink);
    }
    
    /** @test */
    public function first_and_last_items_dont_show_previous_and_last_pagination_links_respectively()
    {
        $firstItem = \App\Item::find(1);
        $secondItem = \App\Item::find(2);
        $thirdItem = \App\Item::find(3);
    
        $nextLink = '<a href="'.route('items.show', $secondItem).'" class="btn-pager-next';
        $this->get(route('items.show', $firstItem))
            ->assertDontSee('class="btn-pager-prev')
            ->assertSee($nextLink);
    
        $previousLink = '<a href="'.route('items.show', $secondItem).'" class="btn-pager-prev';
        $this->get(route('items.show', $thirdItem))
            ->assertSee($previousLink)
            ->assertDontSee('class="btn-pager-next');
    }
    
    /**
     * Create some campaigns as an admin user with some related items
     */
    public function createCampaignsWithRelatedItems()
    {
        $this->createBasicItems();
        
        $data1 = ['title' => 'First title', 'body' => 'Some first body.', 'items' => [1, 2, 3], 'published' => true];
        $this->post(route('admin.campaigns.store'), $data1);
    
        $data2 = ['title' => 'Second title', 'body' => 'Some other body.', 'items' => [4], 'published' => true];
        $this->post(route('admin.campaigns.store'), $data2);
    }
    
    /**
     * Create some basic items of diverse types
     */
    public function createBasicItems()
    {
        $imageFile1 = UploadedFile::fake()->image('niceimage.jpg', 600, 600);
        $this->createItem(['title' => 'A first item', 'item_type_id' => 1, 'featured_image' => $imageFile1, 'published' => true]);
    
        $imageFile2 = UploadedFile::fake()->image('niceimage2.jpg', 600, 600);
        $this->createItem(['title' => 'A second item', 'item_type_id' => 1, 'featured_image' => $imageFile2, 'published' => true]);
    
        $imageFile3 = UploadedFile::fake()->image('niceimage3.jpg', 600, 600);
        $this->createItem(['title' => 'A third item', 'item_type_id' => 1, 'featured_image' => $imageFile3, 'published' => true]);
    
        $imageFile4 = UploadedFile::fake()->image('niceimage4.jpg', 600, 600);
        $this->createItem(['title' => 'A fourth item', 'item_type_id' => 1, 'featured_image' => $imageFile4, 'published' => true]);
    }
    
    /**
     * Create an item as an admin user
     */
    public function createItem(array $data)
    {
        $this->withExceptionHandling()->signInAsAdmin();
        
        return $this->post(route('admin.items.store'), $data);
    }
}
