<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ItemImageUploadTest extends TestCase
{
    use RefreshDatabase;
    
    public function setUp()
    {
        parent::setUp();
    
        $this->createItemTypes();
        
        Storage::fake('public');
    }
    
    /** @test */
    public function an_invalid_file_gives_a_form_error()
    {
        $this->createBasicItemWithImage('non-valid-file')
            ->assertSessionHasErrors(['featured_image']);
    }
    
    /** @test */
    public function an_invalid_image_file_gives_a_form_error()
    {
        $noImageFile = UploadedFile::fake()->create('wrongfile.txt', 200);
        
        $this->createBasicItemWithImage($noImageFile)
            ->assertSessionHasErrors(['featured_image']);
    }
    
    /** @test */
    public function a_valid_image_file_is_uploaded_and_saved()
    {
        $imageFile = UploadedFile::fake()->image('niceimage.jpg', 600, 600);
        
        $this->createBasicItemWithImage($imageFile);
        
        $item = \App\Item::first();
        
        $this->assertTrue($item->hasMedia('featured'));
    }
    
    /** @test */
    public function all_necessary_image_conversions_are_saved()
    {
        $imageFile = UploadedFile::fake()->image('bigimage.jpg', 1600, 1600);
        
        $this->createBasicItemWithImage($imageFile);
        
        $item = \App\Item::first();
        
        $conversionNames = ['item-teaser'];
        
        foreach ($conversionNames as $conversionName) {
            $this->assertNotEmpty($item->getFirstMediaUrl('featured', $conversionName));
        }
    }
    
    /**
     * Create an item as an admin user with an image attached
     */
    public function createBasicItemWithImage($featuredImageField)
    {
        $this->withExceptionHandling()->signInAsAdmin();
    
        $data = ['title' => 'A new item', 'item_type_id' => 1, 'featured_image' => $featuredImageField, 'published' => true];
    
        return $this->post(route('admin.items.store'), $data);
    }
}
