<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DistrictTest extends TestCase
{
    use RefreshDatabase;
    
    public function setUp()
    {
        parent::setUp();
        
        $this->createDistricts();
    }
    
    /** @test */
    public function it_categorizes_many_campaigns()
    {
        $categorizedCampaigns = $this->createCampaignsAndAssignFirstDistrict();
        
        $this->assertCount(2, $categorizedCampaigns);
        
        $this->assertEquals($categorizedCampaigns[0]->title, 'First campaign');
        $this->assertEquals($categorizedCampaigns[1]->title, 'Second campaign');
    }
    
    public function createCampaignsAndAssignFirstDistrict()
    {
        $user = create('App\User');
        
        $campaign1 = $user->campaigns()->create(['title' => 'First campaign', 'published' => true]);
        $campaign1->districts()->attach(1);
        
        $campaign2 = $user->campaigns()->create(['title' => 'Second campaign', 'published' => true]);
        $campaign2->districts()->attach(1);
        
        return \App\District::first()->campaigns;
    }
}
