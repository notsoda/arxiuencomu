<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
    use RefreshDatabase;
    
    public function setUp()
    {
        parent::setUp();
        
        $this->createCategories();
    }
    
    /** @test */
    public function it_categorizes_many_campaigns()
    {
        $categorizedCampaigns = $this->createCampaignsAndAssignFirstCategory();
        
        $this->assertCount(2, $categorizedCampaigns);
    
        $this->assertEquals($categorizedCampaigns[0]->title, 'First campaign');
        $this->assertEquals($categorizedCampaigns[1]->title, 'Second campaign');
    }
    
    public function createCampaignsAndAssignFirstCategory()
    {
        $user = create('App\User');
    
        $campaign1 = $user->campaigns()->create(['title' => 'First campaign', 'published' => true]);
        $campaign1->categories()->attach(1);
    
        $campaign2 = $user->campaigns()->create(['title' => 'Second campaign', 'published' => true]);
        $campaign2->categories()->attach(1);
        
        return \App\Category::first()->campaigns;
    }
}
