<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CampaignTest extends TestCase
{
    use RefreshDatabase;
    
    protected $campaign;
    
    public function setUp()
    {
        parent::setUp();
        
        $this->campaign = $this->createCampaign();
    }
    
    /** @test */
    public function it_belongs_to_a_user()
    {
        $this->assertNotNull($this->campaign->user);
        
        $this->assertInstanceOf('App\User', $this->campaign->user);
    }
    
    /** @test */
    public function it_may_belong_to_many_categories()
    {
        $this->createCategories();
        
        $this->assertEmpty($this->campaign->categories);
    
        $this->campaign->categories()->sync([1, 2]);
    
        $campaignCategories = $this->campaign->fresh()->categories;
        
        $this->assertCount(2, $campaignCategories);
        
        $this->assertEquals($campaignCategories[0]->name, 'First category');
        $this->assertEquals($campaignCategories[1]->name, 'Second category');
    }
    
    /** @test */
    public function it_may_belong_to_many_districts()
    {
        $this->createDistricts();
        
        $this->assertEmpty($this->campaign->districts);
        
        $this->campaign->districts()->sync([1, 2, 4]);
        
        $campaignDistricts = $this->campaign->fresh()->districts;
        
        $this->assertCount(3, $campaignDistricts);
        
        $this->assertEquals($campaignDistricts[0]->name, 'Ciutat Vella');
        $this->assertEquals($campaignDistricts[1]->name, 'Eixample');
        $this->assertEquals($campaignDistricts[2]->name, 'Horta-Guinardó');
    }
    
    /** @test */
    public function it_may_belong_to_many_tags()
    {
        $this->createTags();
        
        $this->assertEmpty($this->campaign->tags);
        
        $this->campaign->tags()->sync([1, 3, 5]);
        
        $campaignTags = $this->campaign->fresh()->tags;
        
        $this->assertCount(3, $campaignTags);
        
        $this->assertEquals($campaignTags[0]->name, 'tag1');
        $this->assertEquals($campaignTags[1]->name, 'tag3');
        $this->assertEquals($campaignTags[2]->name, 'tag5');
    }
    
    public function createCampaign()
    {
        $user = create('App\User');
        
        return $user->campaigns()->create(['title' => 'First campaign', 'published' => true]);
    }
}
