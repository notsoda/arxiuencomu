<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TagTest extends TestCase
{
    use RefreshDatabase;
    
    public function setUp()
    {
        parent::setUp();
        
        $this->createTags();
    }
    
    /** @test */
    public function it_categorizes_many_campaigns()
    {
        $categorizedCampaigns = $this->createCampaignsAndAssignFirstTag();
        
        $this->assertCount(2, $categorizedCampaigns);
        
        $this->assertEquals($categorizedCampaigns[0]->title, 'First campaign');
        $this->assertEquals($categorizedCampaigns[1]->title, 'Second campaign');
    }
    
    public function createCampaignsAndAssignFirstTag()
    {
        $user = create('App\User');
        
        $campaign1 = $user->campaigns()->create(['title' => 'First campaign', 'published' => true]);
        $campaign1->tags()->attach(1);
        
        $campaign2 = $user->campaigns()->create(['title' => 'Second campaign', 'published' => true]);
        $campaign2->tags()->attach(1);
        
        return \App\Tag::first()->campaigns;
    }
}
