<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    
    protected $oldExceptionHandler;
    
    protected function setUp()
    {
        parent::setUp();
        
        Schema::enableForeignKeyConstraints();
        
        $this->withoutExceptionHandling();
    
        \App\Role::create(['name' => 'admin']);
    }
    
    protected function signIn($user = null)
    {
        $user = $user ?: create('App\User');
        
        $this->be($user);
        
        return $this;
    }
    
    protected function signInAsAdmin()
    {
        $adminUser = create('App\User');
        $adminUser->role()->associate(\App\Role::first());
        
        $this->signIn($adminUser);
        
        return $this;
    }
    
    protected function logout()
    {
        Auth::logout();
    }
    
    public function createCategories()
    {
        \App\Category::create(['name' => 'First category']);
        \App\Category::create(['name' => 'Second category']);
        \App\Category::create(['name' => 'Third category']);
    }
    
    protected function createDistricts()
    {
        \App\District::create(['name' => 'Ciutat Vella']);
        \App\District::create(['name' => 'Eixample']);
        \App\District::create(['name' => 'Gràcia']);
        \App\District::create(['name' => 'Horta-Guinardó']);
    }
    
    protected function createTags()
    {
        \App\Tag::create(['name' => 'tag1']);
        \App\Tag::create(['name' => 'tag2']);
        \App\Tag::create(['name' => 'tag3']);
        \App\Tag::create(['name' => 'tag4']);
        \App\Tag::create(['name' => 'tag5']);
        \App\Tag::create(['name' => 'tag6']);
    }
    
    protected function createItemTypes()
    {
        \App\ItemType::create(['name' => 'Banner']);
        \App\ItemType::create(['name' => 'Link']);
        \App\ItemType::create(['name' => 'Video']);
    }
    
    protected function createItems()
    {
        \App\Item::create(['title' => 'A new item', 'item_type_id' => 1, 'published' => true]);
        \App\Item::create(['title' => 'Another item', 'item_type_id' => 1, 'published' => true]);
        \App\Item::create(['title' => 'And now a news item', 'item_type_id' => 3, 'published' => true]);
    }
    
    protected static function getMessyHTML()
    {
        return '<script>alert("This is awful!")</script><a href="#" onclick="alert(\'This is still awful!!\')">Click here!</a>';
    }
}
