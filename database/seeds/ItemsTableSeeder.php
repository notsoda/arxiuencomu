<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Item::create(['title' => 'A new item', 'item_type_id' => 1, 'published' => true]);
        \App\Item::create(['title' => 'Another item', 'item_type_id' => 1, 'published' => true]);
        \App\Item::create(['title' => 'A third item', 'item_type_id' => 1, 'published' => true]);
    }
}
