<?php

use Illuminate\Database\Seeder;

class ItemTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\ItemType::create(['name' => 'Banners']);
        \App\ItemType::create(['name' => 'Vídeos']);
        \App\ItemType::create(['name' => 'Notícies']);
    }
}
