<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create admin role
        \App\Role::create(['name' => 'admin']);
        
        // create initial users
        factory(App\User::class)->create([
            'name' => 'Notsoda',
            'email' => config('app.admin_email'),
            'password' => bcrypt(config('app.admin_password'))
        ]);
        factory(App\User::class)->create([
            'name' => 'Test',
            'email' => 'test@test.test',
        ]);
        
        // assign first user to the admin role
        \App\User::first()->role()->associate(\App\Role::first())->save();
    }
}
