<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Category::create(['name' => 'Habitatge']);
        \App\Category::create(['name' => 'Ecologia']);
        \App\Category::create(['name' => 'Democracia i participació']);
        \App\Category::create(['name' => 'Economia']);
        \App\Category::create(['name' => 'Feminismes']);
        \App\Category::create(['name' => 'Drets socials']);
    }
}
