<?php

use Illuminate\Database\Seeder;

class CampaignsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create demo campaigns
        factory(App\Campaign::class, 15)->create()->each(function ($campaign) {
            // and assign them to a random category
            $categoriesIds = \App\Category::all()->pluck('id');
            $choosenId = $categoriesIds->random();
            $campaign->categories()->attach($choosenId);
            // randomly assign another random category, except the last given id
            if (rand(1, 2) % 2 === 0) {
                $categoriesIds->forget($choosenId-1);
                $choosenId = $categoriesIds->random();
                $campaign->categories()->attach($choosenId);
            }
    
            // and randomly assign them to a random district
            $districtsIds = \App\District::all()->pluck('id');
            if (rand(1, 2) % 2 === 0) {
                $choosenId = $districtsIds->random();
                $campaign->districts()->attach($choosenId);
                // randomly assign another random district, except the last given id
                if (rand(1, 2) % 2 === 0) {
                    $districtsIds->forget($choosenId-1);
                    $choosenId = $districtsIds->random();
                    $campaign->districts()->attach($choosenId);
                }
            }
        });
    }
}
