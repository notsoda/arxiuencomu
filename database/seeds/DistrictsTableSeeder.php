<?php

use Illuminate\Database\Seeder;

class DistrictsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\District::create(['name' => 'Ciutat Vella']);
        \App\District::create(['name' => 'Eixample']);
        \App\District::create(['name' => 'Gràcia']);
        \App\District::create(['name' => 'Horta-Guinardó']);
        \App\District::create(['name' => 'Les Corts']);
        \App\District::create(['name' => 'Nou Barris']);
        \App\District::create(['name' => 'Sant Andreu']);
        \App\District::create(['name' => 'Sant Martí']);
        \App\District::create(['name' => 'Sants-Montjuïc']);
        \App\District::create(['name' => 'Sarrià-Sant Gervasi']);
    }
}
