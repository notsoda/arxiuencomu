<?php

use Faker\Generator as Faker;

$factory->define(App\Campaign::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'date' => \Carbon\Carbon::instance($faker->dateTimeBetween('-4 years'))->format('Y-m-d'),
    ];
});
