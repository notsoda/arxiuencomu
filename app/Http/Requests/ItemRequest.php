<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'required',
                'string',
                'max:255',
            ],
            'item_type_id' => [
                'required',
                'exists:item_types,id'
            ],
            'featured_image' => [
                'nullable',
                'mimes:jpg,jpeg,gif,png'
            ],
            'url' => [
                'nullable',
                'url',
                'max:255',
            ],
            'source' => [
                'nullable',
                'string',
                'max:255',
            ],
            'body' => [
                'nullable',
                'string',
                'max:255',
            ],
            'video_url' => [
                'nullable',
                'string',
                'max:255',
            ],
            'published' => [
                'required',
                'boolean',
            ],
        ];
    }
}
