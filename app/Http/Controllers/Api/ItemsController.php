<?php

namespace App\Http\Controllers\Api;

use App\Campaign;
use App\Http\Controllers\Controller;
use App\Item;

class ItemsController extends Controller
{
    /**
     * Wait some fractions of a second to give some feedback to the user that something is being loaded,
     * as we assume that this api call is going to be too fast for the user to notice anything was really done
     */
    private function microWait()
    {
        usleep(300000);
    }
    
    /**
     * Returns the items to show in the Campaign creation items reference
     *
     * @return \Illuminate\Http\Response
     */
    public function creation()
    {
        $this->microWait();
        return Item::unasigned();
    }
    
    /**
     * Returns the items to show in the Campaign edition items reference
     *
     * @return \Illuminate\Http\Response
     */
    public function edition(Campaign $campaign)
    {
        $this->microWait();
        return Item::unasigned()->merge($campaign->items);
    }
}
