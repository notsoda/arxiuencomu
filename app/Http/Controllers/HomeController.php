<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Services\SearchCampaignService;

class HomeController extends Controller
{
    public function index(SearchCampaignService $search)
    {
        if ($search->isEmpty()) {
            $campaigns = Campaign::lastCampaignsByDate();
        } else {
            $campaigns = $search->start();
    
            if (request()->expectsJson()) return $campaigns->get();
        }
    
        return view('home', ['campaigns' => $campaigns->paginate(12), 'searching' => !$search->isEmpty(), 'searchQuery' => $search->getAllValues()]);
    }
}
