<?php

namespace App\Http\Controllers;

use App\Item;
use App\ItemType;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        $previousItem = $item->previous;
        $nextItem = $item->next;

        $shareableUrl = $item->url;
        switch ($item->item_type_id) {
            case ItemType::TYPE_BANNER:
                $shareableUrl = $item->getFirstMedia('featured')->getFullUrl();
                break;
            case ItemType::TYPE_VIDEO:
                $shareableUrl = $item->video_url;
                break;
        }
    
        $goBackUri = route('home', ['q' => $item->campaign->title]);
    
        return view('items.show', compact('item', 'previousItem', 'nextItem', 'shareableUrl', 'goBackUri'));
    }
    
    /**
     * Download the associated image
     *
     * @param Item $item
     * @return \Illuminate\Http\Response
     */
    public function download(Item $item)
    {
        $mediaItem = $item->getFirstMedia('featured');
        
        if ($mediaItem) {
            return response()->download($mediaItem->getPath(), $mediaItem->file_name);
        }
    
        abort(404, __('interface.page_not_found'));
    }
}
