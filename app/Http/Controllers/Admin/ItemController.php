<?php

namespace App\Http\Controllers\Admin;

use App\ItemType;
use App\Http\Controllers\Controller;
use App\Http\Requests\ItemRequest;
use App\Item;

class ItemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::latest()->paginate(24);
    
        return view('admin.items.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ItemRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemRequest $request)
    {
        $this->authorize('create', Item::class);
    
        $item = $request->user()->items()->create($request->all());
    
        if (isset($request->featured_image)) {
            $item->addFeaturedImage('featured_image');
        }
        
        if ($item->isVideo) {
            $item->addVideoThumbImage();
        }
        
        if ($item->isNews) {
            $item->addMetaImage();
        }
    
        if (request()->wantsJson()) {
            return response($item, 201);
        }
        
        return redirect(route('admin.items.index'))
            ->with('flash', __('interface.admin.item.created'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        $itemTypes = ItemType::all();
        
        $currentImageUrl = $item->getFirstMediaUrl('featured', 'item-teaser');
        
        return view('admin.items.edit', compact('item', 'itemTypes', 'currentImageUrl'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ItemRequest  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(ItemRequest $request, Item $item)
    {
        $this->authorize('update', $item);
    
        if ($item->isVideo) {
            $videoUrlChanged = false;
            // if video url has changed, remove old image
            if ($item->video_url !== $request->video_url) {
                $videoUrlChanged = true;
                $item->removeVideoThumbImage();
                
            }
        }
    
        if ($item->isNews) {
            $urlChanged = false;
            // if url has changed, remove old image
            if ($item->url !== $request->url) {
                $urlChanged = true;
                $item->removeMetaImage();
            
            }
        }
        
        $item->update($request->all());
    
        if (isset($request->featured_image)) {
            $item->removeFeaturedImage();
            $item->addFeaturedImage('featured_image');
        } else {
            if (isset($request->delete_featured_image)) {
                $item->removeFeaturedImage();
            }
        }
    
        if ($item->isVideo) {
            // if video url changed, add new image
            if ($videoUrlChanged) {
                $item->addVideoThumbImage();
            }
        }
    
        if ($item->isNews) {
            // if url changed, add new image
            if ($urlChanged) {
                $item->addMetaImage();
            }
        }
    
        return redirect(route('admin.items.index'))
            ->with('flash', __('interface.admin.item.updated'));
    }
    
    /**
     * Show confirmation to delete the resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function delete(Item $item)
    {
        return view('admin.items.delete', compact('item'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        $this->authorize('delete', $item);
        
        $item->delete();
    
        return redirect(route('admin.items.index'))
            ->with('flash', __('interface.admin.item.deleted'));
    }
}
