<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Requests\UpdateUserRequest;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;

class UserController extends RegisterController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    
        $this->middleware = [];
        
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(24);
    
        return view('admin.users.index', compact('users'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('admin.users.create');
    }
    
    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->authorize('create', User::class);
        
        $this->validator($request->all())->validate();
        
        event(new Registered($user = $this->create($request->all())));
        
        return redirect(route('admin.users.index'))
            ->with('flash', __('interface.admin.user.created'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $this->authorize('update', $user);
        
        $user->update($request->all());
        
        return redirect(route('admin.users.index', $user))
            ->with('flash', __('interface.admin.user.updated'));
    }
    
    /**
     * Show the form for destroying the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function delete(User $user)
    {
        return view('admin.users.delete', compact('user'));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        try {
            $this->authorize('delete', $user);
        } catch (\Exception $e) {
            return redirect(route('admin.users.index'))
                ->with('flash', __('interface.admin.user.cannot_delete_self'));
        }
        
        $user->delete();
        
        return redirect(route('admin.users.index'))
            ->with('flash', __('interface.admin.user.deleted'));
    }
}
