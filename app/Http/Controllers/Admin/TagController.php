<?php

namespace App\Http\Controllers\Admin;

use App\Tag;
use App\Http\Controllers\Controller;
use App\Http\Requests\TagRequest;

class TagController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::all();
        
        return view('admin.tags.index', compact('tags'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\TagRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagRequest $request)
    {
        $this->authorize('create', Tag::class);
        
        Tag::create($request->all());
        
        return redirect(route('admin.tags.index'))
            ->with('flash', __('interface.admin.tag.created'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        return view('admin.tags.edit', compact('tag'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\TagRequest  $request
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(TagRequest $request, Tag $tag)
    {
        $this->authorize('update', $tag);
        
        $tag->update($request->all());
        
        return redirect(route('admin.tags.index'))
            ->with('flash', __('interface.admin.tag.updated'));
    }
    
    /**
     * Show confirmation to delete the resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function delete(Tag $tag)
    {
        return view('admin.tags.delete', compact('tag'));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        $this->authorize('delete', $tag);
    
        $tag->campaigns()->detach();
        
        $tag->delete();
        
        return redirect(route('admin.tags.index'))
            ->with('flash', __('interface.admin.tag.deleted'));
    }
}
