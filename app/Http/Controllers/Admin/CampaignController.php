<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\District;
use App\Http\Controllers\Controller;
use App\Http\Requests\CampaignRequest;
use App\Campaign;
use App\Item;
use App\Tag;

class CampaignController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $campaigns = Campaign::latest()->paginate(24);
    
        return view('admin.campaigns.index', compact('campaigns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        
        $districts = District::all();
    
        $tags = Tag::all();
        
        $items = Item::unasigned();
        
        return view('admin.campaigns.create', compact('categories', 'districts', 'tags', 'items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CampaignRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CampaignRequest $request)
    {
        $this->authorize('create', Campaign::class);
    
        $campaign = Campaign::withoutSyncingToSearch(function () use ($request) {
            return $request->user()->campaigns()->create($request->except(['category_id', 'district_id', 'tag_id', 'items']));
        });
    
        if ($request->category_id) {
            $campaign->categories()->sync($request->category_id);
        }
    
        if ($request->district_id) {
            $campaign->districts()->sync($request->district_id);
        }
    
        if ($request->tag_id) {
            $campaign->tags()->sync($request->tag_id);
        }
        
        if ($request->items) {
            $items = Item::find($request->items);
            if ($items->isNotEmpty()) {
                $campaign->items()->saveMany($items);
            }
        }
        
        $campaign->searchable();
        
        return redirect(route('admin.campaigns.index'))
            ->with('flash', __('interface.admin.campaign.created'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function edit(Campaign $campaign)
    {
        $categories = Category::all();
    
        $districts = District::all();
    
        $tags = Tag::all();
    
        $items = Item::unasigned()->merge($campaign->items);
        
        return view('admin.campaigns.edit', compact('campaign', 'categories', 'districts', 'tags', 'items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CampaignRequest  $request
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function update(CampaignRequest $request, Campaign $campaign)
    {
        $this->authorize('update', $campaign);
    
        Campaign::withoutSyncingToSearch(function () use ($request, $campaign) {
            $campaign->update($request->except(['category_id', 'district_id', 'tag_id', 'items']));
        });
    
        if ($request->category_id) {
            $campaign->categories()->sync($request->category_id);
        }
    
        if ($request->district_id) {
            $campaign->districts()->sync($request->district_id);
        }
    
        if ($request->tag_id) {
            $campaign->tags()->sync($request->tag_id);
        }
    
        $oldItemIds = $campaign->items()->pluck('id');  // [1]
        $newItemIds = collect($request->items ? $request->items : []);
        $forgottenItemIds = $oldItemIds->diff($newItemIds);
        if ($forgottenItemIds->isNotEmpty()) {
            Item::find($forgottenItemIds)->each(function ($item, $key) {
                $item->campaign()->dissociate();
                $item->save();
            });
        }
        if ($newItemIds->isNotEmpty()) {
            $newItems = Item::find($newItemIds);
            $campaign->items()->saveMany($newItems);
        }
    
        $campaign->searchable();
    
        return redirect(route('admin.campaigns.index'))
            ->with('flash', __('interface.admin.campaign.updated'));
    }
    
    /**
     * Show confirmation to delete the resource.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function delete(Campaign $campaign)
    {
        return view('admin.campaigns.delete', compact('campaign'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function destroy(Campaign $campaign)
    {
        $this->authorize('delete', $campaign);
        
        $campaign->categories()->detach();
        $campaign->districts()->detach();
        $campaign->tags()->detach();
        
        $campaign->items->each(function ($item, $key) {
            $item->campaign()->dissociate();
            $item->save();
        });
        
        $campaign->delete();
    
        return redirect(route('admin.campaigns.index'))
            ->with('flash', __('interface.admin.campaign.deleted'));
    }
}
