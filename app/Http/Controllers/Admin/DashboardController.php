<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Campaign;
use App\Item;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lastCampaigns = Campaign::latest()->take(5)->get();
        
        $lastItems = Item::latest()->take(5)->get();
        
        return view('admin.dashboard', compact('lastCampaigns', 'lastItems'));
    }
}