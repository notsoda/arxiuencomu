<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Item extends Model implements HasMedia
{
    use Sluggable, HasMediaTrait, HasFeaturedImage, HasVideoUrl;
    
    protected $fillable = ['title', 'item_type_id', 'url', 'source', 'body', 'video_url', 'published'];
    
    protected $with = ['media'];
    
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
    
    public function getRouteKeyName()
    {
        return 'slug';
    }
    
    /**
     * Scope a query to only include published content.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->where('published', '=', true);
    }
    
    public function getSummaryAttribute()
    {
        return str_limit($this->body, 100);
    }
    
    /**
     * Checks if it is a banner item
     */
    public function getIsBannerAttribute()
    {
        return ((int)$this->item_type_id === ItemType::TYPE_BANNER);
    }
    
    /**
     * Checks if it is a video item
     */
    public function getIsVideoAttribute()
    {
        return ((int)$this->item_type_id === ItemType::TYPE_VIDEO);
    }
    
    /**
     * Checks if it is a news item
     */
    public function getIsNewsAttribute()
    {
        return ((int)$this->item_type_id === ItemType::TYPE_NEWS);
    }
    
    /**
     * Get the previous published item in the campaign.
     */
    public function getPreviousAttribute()
    {
        return $this->getFollowing(false);
    }
    
    /**
     * Get the next published item in the campaign.
     */
    public function getNextAttribute()
    {
        return $this->getFollowing();
    }
    
    /**
     * Get the previous or next published item in the campaign, which not of type News (these do not have detail).
     */
    protected function getFollowing(bool $isNext = true)
    {
        $order = $isNext ? 'asc' : 'desc';
        $dateComparison = $isNext ? '>' : '<';
        
        return Item::published()
            ->where([
                ['campaign_id', '=', $this->campaign_id],
                ['id', $dateComparison, $this->id],
                ['item_type_id', '<>', ItemType::TYPE_NEWS]
            ])
            ->orderBy('id', $order)
            ->take(1)
            ->get()
            ->first();
    }
    
    /**
     * Takes the item url's and parses its url's image metatag, and then saves that as an image
     */
    public function addMetaImage()
    {
        if ($this->url) {
            $imageUri = $this->getWebMetaOGImage($this->url);
            if ($imageUri) {
                $this->addMediaFromUrl($imageUri, 'image/jpeg', 'image/png')->toMediaCollection('external');
            }
        }
    }
    
    /**
     * Gets the og:image metatag url from the passed website's url
     * @todo Put this in a service, not here in the model
     *
     * @param string $url
     * @return string image url or null
     */
    public function getWebMetaOGImage(string $url)
    {
        $doc = new \DomDocument();
        @$doc->loadHTML(file_get_contents($url));
        $xpath = new \DOMXPath($doc);
        $query = '//*/meta[starts-with(@property, \'og:\')]';
        $metas = $xpath->query($query);
        foreach ($metas as $meta) {
            if($meta->getAttribute('property') === 'og:image'){
                return $meta->getAttribute('content');
            }
        }
        
        return null;
    }
    
    public function removeMetaImage()
    {
        $this->clearMediaCollection('external');
    }
    
    /**
     * Get published items not assigned to any campaign.
     */
    public static function unasigned()
    {
        return self::published()->orderBy('updated_at', 'desc')->get()->filter(function($item){
            return (is_null($item->campaign));
        });
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function itemType()
    {
        return $this->belongsTo(ItemType::class);
    }
    
    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }
    
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('item-teaser')
            ->keepOriginalImageFormat()
            ->width(600);
    
        $this->addMediaConversion('item-url-teaser')
            ->keepOriginalImageFormat()
            ->height(300)
            ->crop(Manipulations::CROP_TOP, 600, 300);
    }
}
