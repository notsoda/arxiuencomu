<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    
    protected $fillable = ['name', 'email', 'password'];
    
    protected $hidden = ['password', 'remember_token'];
    
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
    
    public function campaigns()
    {
        return $this->hasMany(Campaign::class);
    }
    
    public function items()
    {
        return $this->hasMany(Item::class);
    }
    
    public function isAdmin()
    {
        return ($this->role && $this->role->id === 1);
    }
}
