<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Campaign extends Model
{
    use Sluggable, Dateable, Searchable;
    
    protected $fillable = ['title', 'date', 'published'];
    
    protected $with = ['categories', 'districts', 'tags', 'items'];
    
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
    
    public function getRouteKeyName()
    {
        return 'slug';
    }
    
    /**
     * Determine if the model should be searchable.
     *
     * @return bool
     */
    public function shouldBeSearchable()
    {
        return $this->published;
    }
    
    /**
     * Scope a query to only include published content.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->where('published', '=', true);
    }
    
    public static function lastCampaignsByDate(int $max = null)
    {
        $campaigns = static::select('campaigns.*')
            ->orderBy('date', 'desc')
            ->published()
            ->take($max);
        
        return $campaigns;
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
    
    public function districts()
    {
        return $this->belongsToMany(District::class);
    }
    
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
    
    public function items()
    {
        return $this->hasMany(Item::class);
    }
}
