<?php

namespace App;

use Illuminate\Support\Facades\Log;
use MediaEmbed\MediaEmbed;
use Merujan99\LaravelVideoEmbed\Facades\LaravelVideoEmbed;

trait HasVideoUrl
{
    /**
     * Takes the item video url, gets its video thumbnail and then saves that as an image
     */
    public function addVideoThumbImage()
    {
        if ($this->video_url) {
            $thumbUrl = $this->getYoutubeThumbnailOfSize($this->video_url);
            if ($thumbUrl) {
                try {
                    $this->addMediaFromUrl($thumbUrl, 'image/jpeg', 'image/png')->toMediaCollection('youtube');
                } catch (\Exception $exception) {
                    Log::debug('The url ' . $thumbUrl . ' could not be loaded.');
                }
            }
        }
    }
    
    public function getYoutubeThumbnailOfSize($url = null, $size = 'mqdefault')
    {
        $MediaEmbed = new MediaEmbed();
        
        $MediaObject = $MediaEmbed->parseUrl($url);
        
        if($MediaObject) {
            return "https://img.youtube.com/vi/{$MediaObject->id()}/{$size}.jpg";
        }
        
        return false;
    }
    
    public function removeVideoThumbImage()
    {
        $this->clearMediaCollection('youtube');
    }
    
    /**
     * Get the youtube embed code (iframe) if the item has a video url
     */
    public function getVideoEmbedAttribute()
    {
        if ($this->video_url) {
            $whitelist = ['YouTube'];
            
            return LaravelVideoEmbed::parse($this->video_url, $whitelist);
        }
    }
}