<?php

namespace App\Providers;

use App\Services\ShareLinksService;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('admin.items._createformfields', function ($view) {
            $view->with('itemTypes', \App\ItemType::orderBy('created_at', 'asc')->get());
        });
    
        View::composer('search._filters', function ($view) {
            $view->with('categories', \App\Category::all());
            $view->with('itemTypes', \App\ItemType::all());
            $view->with('districts', \App\District::all());
        });
    
        // pagination views
        Paginator::defaultView('pagination::default');
        Paginator::defaultSimpleView('pagination::simple-default');
        
        // share links
        View::composer('partials._sharelinks', function ($view) {
            $view->with('links', ShareLinksService::getLinks());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
