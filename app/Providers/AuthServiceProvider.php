<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Campaign' => 'App\Policies\CampaignPolicy',
        'App\Category' => 'App\Policies\CategoryPolicy',
        'App\Tag' => 'App\Policies\TagPolicy',
        'App\User' => 'App\Policies\UserPolicy',
        'App\Item' => 'App\Policies\ItemPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
