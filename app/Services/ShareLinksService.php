<?php

namespace App\Services;

class ShareLinksService
{
    public static function getLinks()
    {
        return [
            'facebook' => [
                'name' => 'Facebook',
                'shareUrl' => 'https://www.facebook.com/sharer/sharer.php?u=',
                'label' => __("Share"),
                'description' => __("Share on Facebook"),
            ],
            'twitter' => [
                'name' => 'Twitter',
                'shareUrl' => 'https://twitter.com/intent/tweet?url=',
                'label' => __("Tweet"),
                'description' => __("Tweet"),
            ],
            'watsapp' => [
                'name' => 'Watsapp',
                'shareUrl' => 'https://api.whatsapp.com/send?text=',
                'label' => __("Share"),
                'description' => __("Share on Watsapp"),
            ],
            'telegram' => [
                'name' => 'Telegram',
                'shareUrl' => 'https://telegram.me/share/url?url=',
                'label' => __("Share"),
                'description' => __("Share on Telegram"),
            ],
        ];
    }
}