<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Campaign;

class SearchCampaignService
{
    public $allQuery = [
        'q' => [],
        'cat' => ['facetName' => 'categories.id'],
        'cont' => ['facetName' => 'items.item_type_id'],
        'dist' => ['facetName' => 'districts.id'],
    ];
    
    public function __construct(Request $request)
    {
        foreach ($this->allQuery as $key => $params) {
            $this->allQuery[$key]['value'] = $request->get($key);
        }
    }
    
    public function isEmpty()
    {
        foreach ($this->allQuery as $key => $params) {
            if ($params['value'] !== null) return false;
        }
        return true;
    }
    
    public function getAllValues()
    {
        $values = [];
        foreach ($this->allQuery as $key => $params) {
            if ($params['value'] !== null) {
                $values[$key] = $params['value'];
            }
        }
        return $values;
    }
    
    public function getValue(string $key)
    {
        if (!is_null($this->allQuery[$key])) {
            return $this->allQuery[$key]['value'];
        }
        return null;
    }
    
    public function start()
    {
        if (count($this->getAllValues()) > 1 || $this->getValue('q') === null) {
            // searching with filters
            return $this->facetSearch();
        }
        return $this->basicSearch();
    }
    
    public function basicSearch()
    {
        return Campaign::search($this->getValue('q'));
    }
    
    public function facetSearch()
    {
        return Campaign::search($this->getValue('q'), function ($algolia, $query, $options) {
            $newOptions = [
                'facetFilters' => []
            ];
    
            foreach ($this->allQuery as $key => $params) {
                if ($key != 'q') {
                    if (!is_null($value = $this->getValue($key))) {
                        $facet = [];
                        foreach ($value as $id) {
                            $facet[] = $params['facetName'] . ':' . $id;
                        }
                        $newOptions['facetFilters'][] = $facet;
                    }
                }
            }
            
            return $algolia->search($query, array_merge($options, $newOptions));
        });
    }
}