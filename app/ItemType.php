<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class ItemType extends Model
{
    use Sluggable;
    
    const TYPE_BANNER = 1;
    const TYPE_VIDEO = 2;
    const TYPE_NEWS = 3;

    protected $fillable = ['name'];
    
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    
    public function getRouteKeyName()
    {
        return 'slug';
    }
    
    public function items()
    {
        return $this->hasMany(Item::class);
    }
}
