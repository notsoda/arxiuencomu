<?php

namespace App;

use Carbon\Carbon;

trait Dateable
{
    public function getHumanDateAttribute()
    {
        return self::convertDateToHumanFormat($this->date);
    }
    
    public static function getDateDatabaseFormat()
    {
        return 'Y-m-d';
    }
    
    public static function convertDateToHumanFormat($date)
    {
        setlocale(LC_TIME, config('app.locale'));
        
        $humanFormat = '%B %Y'; // month year
        
        return Carbon::createFromFormat(self::getDateDatabaseFormat(), $date)->formatLocalized($humanFormat);
    }
}