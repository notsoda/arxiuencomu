<?php

namespace App;

trait HasFeaturedImage
{
    public function addFeaturedImage($fieldName)
    {
        $this->addMediaFromRequest($fieldName)->toMediaCollection('featured');
    }
    
    public function removeFeaturedImage()
    {
        if ($this->hasMedia('featured')) {
            $this->clearMediaCollection('featured');
        }
    }
}