<filters-form inline-template>
    <form method="get" action="{{ route('home') }}" ref="form" @submit.prevent="submitForm">
        <input type="hidden" name="q" value="{{ request('q') }}">

        <div class="block">
            <header>
                <h2>@lang('interface.filters.filter_by') @lang('interface.filters.category')</h2>
            </header>
            <div class="content">
                @foreach($categories as $category)
                    <div class="form-item">
                        <input type="checkbox" value="{{ $category->id }}" id="cat-{{ $category->id }}" name="cat[]" @change="submitForm" @if (request('cat') && in_array($category->id, request('cat'))) checked @endif>
                        <label for="cat-{{ $category->id }}">{{ $category->name }}</label>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="block">
            <header>
                <h2>@lang('interface.filters.filter_by') @lang('interface.filters.content')</h2>
            </header>
            <div class="content">
                @foreach($itemTypes as $type)
                    <div class="form-item">
                        <input type="checkbox" value="{{ $type->id }}" id="cont-{{ $type->id }}" name="cont[]" @change="submitForm" @if (request('cont') && in_array($type->id, request('cont'))) checked @endif>
                        <label for="cont-{{ $type->id }}">{{ $type->name }}</label>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="block">
            <header>
                <h2>@lang('interface.filters.filter_by') @lang('interface.filters.district')</h2>
            </header>
            <div class="content">
                @foreach($districts as $district)
                    <div class="form-item">
                        <input type="checkbox" value="{{ $district->id }}" id="dist-{{ $district->id }}" name="dist[]" @change="submitForm" @if (request('dist') && in_array($district->id, request('dist'))) checked @endif>
                        <label for="dist-{{ $district->id }}">{{ $district->name }}</label>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="form-actions">
            <a href="{{ route('home') }}" data-action="clear-form">@lang('interface.search.clear')</a>
        </div>
    </form>
</filters-form>