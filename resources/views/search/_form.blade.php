<search-form class="search-box" origin-q="{{ request('q') }}"
             inline-template>
    <form method="get" action="{{ route('home') }}" ref="form" @submit.prevent="submitForm">
        @if (request('cat'))
            @foreach(request('cat') as $id)
                <input type="hidden" name="cat[]" value="{{ $id }}">
            @endforeach
        @endif

        @if (request('cont'))
            @foreach(request('cont') as $id)
                <input type="hidden" name="cont[]" value="{{ $id }}">
            @endforeach
        @endif

        @if (request('dist'))
            @foreach(request('dist') as $id)
                <input type="hidden" name="dist[]" value="{{ $id }}">
            @endforeach
        @endif

        <input type="text" name="q" ref="input" value="{{ request('q') }}" placeholder="@lang('interface.search.placeholder')" v-model.trim="q">
    </form>
</search-form>