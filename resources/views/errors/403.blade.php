@extends('layouts.errorpage')

@section('meta-title') Error 403 @endsection

@section('content')
    <div class="main-content error">
        <div class="wrapper">
            <h1>Error 403</h1>

            <p>You don't have permission to see this :S</p>
        </div>
    </div>
@endsection




