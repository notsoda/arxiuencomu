<article class="detail campaign" id="{{ $campaign->id }}">
    <p class="date">{{ $campaign->human_date }}</p>

    <h1>{{ $campaign->title }}</h1>

    @can('update', $campaign)
        <a href="{{ route('admin.campaigns.edit', $campaign) }}" class="edit-btn">Edit</a>
    @endcan

    @if (count($campaign->items) > 0)
        <div class="items-list">
            @if (count($campaign->items) > 0)
                <ul>
                    @foreach($campaign->items as $item)
                        <li>
                            @include('items._teaser')
                        </li>
                    @endforeach
                </ul>
            @else
                <p class="is-empty">@lang('interface.campaign_without_items')</p>
            @endif
        </div>
    @endif

    <div class="taxonomies">
        @if (count($campaign->categories) > 0)
            <div class="taxonomy-list categories-list">
                <ul>
                    @foreach($campaign->categories as $category)
                        <li class="category-name">{{ $category->name }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (count($campaign->districts) > 0)
            <div class="taxonomy-list districts-list">
                <ul>
                    @foreach($campaign->districts as $district)
                        <li class="district-name">{{ $district->name }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</article>