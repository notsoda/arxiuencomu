@csrf

<div class="form-item">
    <label for="name">{{ __('Name') }}</label>
    <input id="name" type="text" class="{{ $errors->has('name') ? ' error' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

    @if ($errors->has('name'))
        <div class="error-message">
            <strong>{{ $errors->first('name') }}</strong>
        </div>
    @endif
</div>

<div class="form-item">
    <label for="email">{{ __('E-Mail Address') }}</label>
    <input id="email" type="email" class="{{ $errors->has('email') ? ' error' : '' }}" name="email" value="{{ old('email') }}" required>

    @if ($errors->has('email'))
        <div class="error-message">
            <strong>{{ $errors->first('email') }}</strong>
        </div>
    @endif
</div>

<div class="form-item">
    <label for="password">{{ __('Password') }}</label>
    <input id="password" type="password" class="{{ $errors->has('password') ? ' error' : '' }}" name="password" required>

    @if ($errors->has('password'))
        <div class="error-message">
            <strong>{{ $errors->first('password') }}</strong>
        </div>
    @endif
</div>

<div class="form-item">
    <label for="password-confirm">{{ __('Confirm Password') }}</label>
    <input id="password-confirm" type="password" name="password_confirmation" required>
</div>

<div class="form-actions">
    <button type="submit">
        {{ __('Register') }}
    </button>
    @can('create', \App\User::class)
        <a href="{{ route('admin.users.index') }}">{{ __('Cancel') }}</a>
    @endcan
</div>