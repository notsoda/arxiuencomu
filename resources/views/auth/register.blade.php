@extends('layouts.app')

@section('content')
    <div class="main-content register">
        <div class="wrapper">
            <h1 class="page-title">{{ __('Register') }}</h1>

            <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                @include('auth.registerfields')
            </form>
        </div>
    </div>
@endsection
