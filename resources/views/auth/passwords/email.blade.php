@extends('layouts.app')

@section('content')
    <div class="main-content reset-password">
        <div class="wrapper">
            <h1 class="page-title">{{ __('Reset Password') }}</h1>

            <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
                @csrf

                <div class="form-item">
                    <label for="email">{{ __('E-Mail Address') }}</label>
                    <input id="email" type="email" class="{{ $errors->has('email') ? ' error' : '' }}" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <div class="error-message">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @endif
                </div>

                <div class="form-actions">
                    <button type="submit">
                        {{ __('Send Password Reset Link') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
