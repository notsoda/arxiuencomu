<div class="item-teaser" data-type="{{ $item->item_type_id }}">
    @if (!$item->isNews)
    <a href="{{ route('items.show', $item) }}" class="main-link">
    @else
    <a href="{{ $item->url }}" target="_blank" class="main-link">
    @endif
        <figure class="image" @if ($item->isBanner) data-mime="{{ $item->getFirstMedia('featured')->mime_type }}" @endif>
            @if ($item->isBanner)
                <img src="{{ $item->getFirstMediaUrl('featured', 'item-teaser') }}" alt="" title="{{ $item->title }}">
            @elseif ($item->isNews)
                <img src="{{ $item->getFirstMediaUrl('external', 'item-url-teaser') }}" alt="" title="{{ $item->title }}">
            @elseif ($item->isVideo)
                @if ($item->getFirstMediaUrl('youtube'))
                    <img src="{{ $item->getFirstMediaUrl('youtube') }}" alt="">
                @else
                    <p>@lang('interface.video_url_wrong')</p>
                @endif
            @endif
        </figure>
        @if ($item->isNews)
            <div class="content">
                @if ($item->source)
                    <p class="source">{{ $item->source }}</p>
                @endif
                <h2>{{ $item->title }}</h2>
                @if ($item->summary)
                    <div class="summary">
                        <p>{{ $item->summary }}</p>
                    </div>
                @endif
            </div>
        @endif
    </a>

    @can('update', $item)
        <a href="{{ route('admin.items.edit', $item) }}" class="edit-btn">Edit</a>
    @endcan
</div>