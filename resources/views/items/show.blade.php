@extends('layouts.app')

@section('meta-title'){{ $item->title }}@endsection

@section('content')
    <div class="main-content item">
        <div class="wrapper">
            <article class="detail item">
                <div class="column-left">
                    <a href="{{ $goBackUri }}" data-action="go-back">@lang('interface.go_back')</a>

                    <aside class="campaign-info">
                        <h2 class="campaign-title">{{ $item->campaign->title }}</h2>
                        <p class="date campaign-date">{{ $item->campaign->human_date }}</p>
                    </aside>
                </div>

                <div class="column-center">
                    <figure class="media">
                        @include('partials._contentpager')

                        @if ($item->isBanner)
                            <a href="{{ $item->getFirstMediaUrl('featured') }}" class="view-full-image">
                                <img src="{{ $item->getFirstMediaUrl('featured') }}" alt="" title="{{ $item->title }}">
                            </a>
                        @elseif ($item->isVideo)
                            @if ($item->videoEmbed)
                                {!! $item->videoEmbed !!}
                            @else
                                <p>@lang('interface.video_url_wrong')</p>
                            @endif
                        @endif
                    </figure>

                    @can('update', $item)
                        <a href="{{ route('admin.items.edit', $item) }}" class="edit-btn">Edit</a>
                    @endcan
                </div>

                <div class="column-right">
                    <div class="block share">
                        <header>
                            <h2>@lang('interface.share')</h2>
                        </header>
                        <div class="content">
                            @include('partials._sharelinks')
                        </div>
                    </div>

                    <a href="{{ route('items.download', $item) }}" data-action="download">@lang('interface.download')</a>
                </div>
            </article>
        </div>
    </div>
@endsection
