@if ($paginator->hasPages())
    <ul class="pagination simple-pagination" role="navigation">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="pager-item pager-previous disabled" aria-disabled="true"><span>@lang('pagination.previous')</span></li>
        @else
            <li class="pager-item pager-previous"><a href="{{ $paginator->previousPageUrl() }}" rel="prev"><span>@lang('pagination.previous')</span></a></li>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="pager-item pager-next"><a href="{{ $paginator->nextPageUrl() }}" rel="next"><span>@lang('pagination.next')</span></a></li>
        @else
            <li class="pager-item pager-next disabled" aria-disabled="true"><span>@lang('pagination.next')</span></li>
        @endif
    </ul>
@endif
