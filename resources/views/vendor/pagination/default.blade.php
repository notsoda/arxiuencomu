@if ($paginator->hasPages())
    <ul class="pagination" role="navigation">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="pager-item pager-previous disabled" aria-disabled="true"><span>@lang('pagination.previous')</span></li>
        @else
            <li class="pager-item pager-previous">
                <a href="{{ $paginator->previousPageUrl() }}" rel="prev"><span>@lang('pagination.previous')</span></a>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="pager-item disabled" aria-disabled="true"><span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="pager-item pager-current" aria-current="page"><span>{{ $page }}</span></li>
                    @else
                        <li class="pager-item"><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="pager-item pager-next"><a href="{{ $paginator->nextPageUrl() }}" rel="next"><span>@lang('pagination.next')</span></a></li>
        @else
            <li class="pager-item pager-next disabled" aria-disabled="true"><span>@lang('pagination.next')</span></li>
        @endif
    </ul>
@endif
