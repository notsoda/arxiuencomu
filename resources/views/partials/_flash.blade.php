@if (session('flash'))
    <div class="flash-message">
        {{ session('flash') }}
    </div>
@endif