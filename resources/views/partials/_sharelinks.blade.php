<div class="share-links">
    <ul>
        @foreach ($links as $machineName => $link)
        <li>
            <a href="{{ $link['shareUrl'] }}{{ $shareableUrl }}" data-social-network="{{ $machineName }}" title="{{ $link['description'] }}">
                <span class="label">{{ $link['label'] }}</span>
            </a>
        </li>
        @endforeach
    </ul>
</div>