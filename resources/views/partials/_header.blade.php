<header class="header">
    <div class="wrapper">
        <a class="logo" href="{{ route('home') }}">
            {{ config('app.name') }}
        </a>

        @include('search._form')
    </div>
</header>
