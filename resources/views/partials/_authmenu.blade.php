<div class="auth-menu">
    <nav>
        <ul>
            @guest
                <li>
                    <a href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
            @else
                @if (Auth::user()->isAdmin())
                    <li>
                        <a href="{{ route('dashboard') }}" class="admin-link">@lang('interface.admin.dashboard.title')</a>
                    </li>
                @endif
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
            @endguest
        </ul>
    </nav>
</div>