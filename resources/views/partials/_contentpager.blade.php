<div class="content-pager">
    @if ($previousItem)
        <a href="{{ route('items.show', $previousItem) }}" class="btn-pager-prev" title="@lang('pagination.previous')"><span>&lt;</span></a>
    @endif
    @if ($nextItem)
        <a href="{{ route('items.show', $nextItem) }}" class="btn-pager-next" title="@lang('pagination.next')"><span>&gt;</span></a>
    @endif
</div>