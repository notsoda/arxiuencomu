@extends('layouts.app')

@section('meta-title'){{ config('app.name') }}@endsection
@section('meta-description')Les campanyes de comunicació en comú.@endsection

@section('content')
    <div class="main-content home">
        <div class="wrapper">
            <div class="column-left column-filters">
                @include('search._filters')
            </div>

            <div class="column-right column-content">
                @if ($searching)
                    @include('search._algoliabrand')
                @endif

                @if (count($campaigns) > 0)
                    <div class="campaigns-list">
                        @foreach($campaigns as $campaign)
                            @include('campaigns._detail')
                        @endforeach
                    </div>

                    {{ $campaigns->appends($searchQuery)->links() }}
                @else
                    <p class="is-empty">
                        @if ($searching)
                            @lang('interface.search.empty')
                        @else
                            @lang('interface.no_campaigns')
                        @endif
                    </p>
                @endif
            </div>
        </div>
    </div>
@endsection