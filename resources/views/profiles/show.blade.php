@extends('layouts.app')

@section('meta-title'){{ $profileUser->name }} - {{ config('app.name') }}@endsection

@section('content')
    <div class="main-content profile">
        <div class="wrapper">
            <h1>{{ $profileUser->name }}</h1>
        </div>
    </div>
@endsection