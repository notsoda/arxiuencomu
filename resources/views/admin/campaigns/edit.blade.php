@extends('admin.layouts.app')

@section('content')
<campaign-form v-cloak
               items-list-uri="{{ route('api.items.edition', $campaign) }}"
               inline-template>
    <div class="campaign-form">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <h1>@lang('interface.admin.campaign.edit')</h1>

                    <form action="{{ route('admin.campaigns.update', $campaign) }}" method="post">
                        @csrf
                        @method('patch')

                        @include('partials._errors')

                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" id="title" class="form-control" value="{{ $campaign->title }}">
                        </div>

                        <div class="form-group">
                            <h2>@lang('interface.category')</h2>
                            @foreach($categories as $category)
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="category-{{ $category->id }}" name="category_id[]" value="{{ $category->id }}" @if (in_array($category->id, $campaign->categories()->pluck('id')->all())) checked @endif>
                                    <label class="form-check-label" for="category-{{ $category->id }}">{{ $category->name }}</label>
                                </div>
                            @endforeach
                        </div>

                        <div class="form-group">
                            <h2>@lang('interface.district')</h2>
                            @foreach($districts as $district)
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="district-{{ $district->id }}" name="district_id[]" value="{{ $district->id }}" @if (in_array($district->id, $campaign->districts()->pluck('id')->all())) checked @endif>
                                    <label class="form-check-label" for="district-{{ $district->id }}">{{ $district->name }}</label>
                                </div>
                            @endforeach
                        </div>

                        <div class="form-group">
                            <h2>@lang('interface.tag')</h2>
                            @foreach($tags as $tag)
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="tag-{{ $tag->id }}" name="tag_id[]" value="{{ $tag->id }}" @if (in_array($tag->id, $campaign->tags()->pluck('id')->all())) checked @endif>
                                    <label class="form-check-label" for="tag-{{ $tag->id }}">{{ $tag->name }}</label>
                                </div>
                            @endforeach
                        </div>

                        <div class="form-group">
                            <label for="date">@lang('interface.campaign_date')</label>
                            <input type="date" name="date" id="date" class="form-control" value="{{ $campaign->date }}">
                        </div>

                        <items-reference v-cloak
                                         :items="items"
                                         :loading="loading"
                                         :selected-items="{{ $campaign->items()->pluck('id') }}"
                                         create-item-uri="{{ route('admin.items.create') }}"
                                         @click-refresh="refreshItemsReference">
                        </items-reference>

                        <div class="form-group">
                            <div class="form-check">
                                <input type="hidden" name="published" value="0">
                                <input class="form-check-input" type="checkbox" id="published" name="published" value="1"@if ($campaign->published) checked @endif>
                                <label class="form-check-label" for="published">@lang('interface.published')</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">@lang('interface.update')</button>
                            @can('delete', $campaign)
                            <a href="{{ route('admin.campaigns.delete', $campaign) }}" class="btn btn-danger">@lang('interface.delete')</a>
                            @endcan
                            <a href="{{ route('admin.campaigns.index') }}" class="btn btn-link">@lang('interface.cancel')</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        @component('admin.components._modal')
            @slot('title')
                @lang('interface.admin.item.create')
            @endslot

            <create-item-form
                    posturi="{{ route('admin.items.store') }}"
                    @item-created="refreshItemsReference"
                    inline-template>
                <form @submit.prevent="submitForm">
                    @include('admin.items._createformfields', ['vuejs' => true])

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">@lang('interface.create')</button>
                    </div>
                </form>
            </create-item-form>
        @endcomponent
    </div>
</campaign-form>
@endsection
