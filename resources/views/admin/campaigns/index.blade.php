@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>@lang('interface.admin.campaign.index')</h1>

                @can('create', \App\Campaign::class)
                    <a href="{{ route('admin.campaigns.create') }}" class="btn btn-primary">@lang('interface.admin.campaign.create')</a>
                @endcan

                <hr>

                @if (count($campaigns) > 0)
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Num items</th>
                            <th scope="col">Published</th>
                            <th scope="col">Author</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($campaigns as $campaign)
                        <tr>
                            <th scope="row">{{ $campaign->id }}</th>
                            <td>{{ str_limit($campaign->title, 90) }}</td>
                            <td>{{ count($campaign->items) }}</td>
                            <td>@if ($campaign->published) <i class="fas fa-check"></i> @else <i class="fas fa-times"></i> @endif</td>
                            <td>{{ $campaign->user->name }}</td>
                            <td>
                                @can('update', $campaign)
                                    <a href="{{ route('admin.campaigns.edit', $campaign) }}" class="btn btn-primary btn-sm">@lang('interface.edit')</a>
                                @endcan
                                @can('delete', $campaign)
                                    <a href="{{ route('admin.campaigns.delete', $campaign) }}" class="btn btn-primary btn-sm btn-danger">@lang('interface.delete')</a>
                                @endcan
                            </td>
                        </tr>
                        @endforeach

                        {{ $campaigns->links() }}
                    </tbody>
                </table>
                @else
                    <p class="is-empty">@lang('interface.no_campaigns')</p>
                @endif
            </div>
        </div>
    </div>
@endsection
