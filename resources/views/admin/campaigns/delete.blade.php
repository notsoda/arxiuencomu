@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <p>@lang('interface.admin.sure_to_delete', ['title' => $campaign->title])</p>

            <form action="{{ route('admin.campaigns.destroy', $campaign) }}" method="post">
                @csrf
                @method('delete')

                <div class="form-group">
                    <button type="submit" class="btn btn-danger">@lang('interface.delete')</button>
                    <a href="{{ route('admin.campaigns.index') }}" class="btn btn-link">@lang('interface.cancel')</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
