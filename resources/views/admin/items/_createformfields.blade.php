@csrf

@include('partials._errors')

<div class="form-group">
    <label for="title">Title</label>
    <input type="text" name="title" id="title" class="form-control" value="{{ old('title') }}" @if ($vuejs) :class="{'is-invalid': errors && errors.title}" v-model="title" @endif>
    @if ($vuejs)
        <div class="invalid-feedback" v-if="errors && errors.title" v-text="errors.title[0]"></div>
    @endif
</div>

<div class="form-group">
    <label for="item_type_id">@lang('interface.item_type')</label>
    <select name="item_type_id" id="item_type_id" class="form-control" @if ($vuejs) :class="{'is-invalid': errors && errors.item_type_id}" v-model="item_type_id" @endif>
        <option value="null">-- @lang('interface.choose_option') --</option>
        @foreach($itemTypes as $type)
            <option value="{{ $type->id }}" @if (!is_null(old('item_type_id')) && $type->id == old('item_type_id')) selected @endif>{{ $type->name }}</option>
        @endforeach
    </select>
    @if ($vuejs)
        <div class="invalid-feedback" v-if="errors && errors.item_type_id" v-text="errors.item_type_id[0]"></div>
    @endif
</div>

<fieldset class="form-group">
    <legend>{{ $itemTypes[\App\ItemType::TYPE_BANNER-1]->name }}</legend>
    <div class="form-group">
        <input type="file" class="form-control-file" name="featured_image" @if ($vuejs) :class="{'is-invalid': errors && errors.featured_image}" @change="onImageChange" @endif>
        @if ($vuejs)
            <div class="invalid-feedback" v-if="errors && errors.featured_image" v-text="errors.featured_image[0]"></div>
        @endif
    </div>
</fieldset>

<fieldset class="form-group">
    <legend>{{ $itemTypes[\App\ItemType::TYPE_VIDEO-1]->name }}</legend>

    <div class="form-group">
        <label for="video_url">URL Video</label>
        <input type="text" name="video_url" id="video_url" size="50" class="form-control" value="{{ old('video_url') }}" @if ($vuejs) :class="{'is-invalid': errors && errors.video_url}" v-model="video_url" @endif>
        <small class="form-text text-muted">@lang('interface.admin.item.video_url.help')</small>
        @if ($vuejs)
            <div class="invalid-feedback" v-if="errors && errors.video_url" v-text="errors.video_url[0]"></div>
        @endif
    </div>
</fieldset>

<fieldset class="form-group">
    <legend>{{ $itemTypes[\App\ItemType::TYPE_NEWS-1]->name }}</legend>

    <div class="form-group">
        <label for="url">Url</label>
        <input type="text" name="url" id="url" class="form-control" value="{{ old('url') }}" @if ($vuejs) :class="{'is-invalid': errors && errors.url}" v-model="url" @endif>
        @if ($vuejs)
            <div class="invalid-feedback" v-if="errors && errors.url" v-text="errors.url[0]"></div>
        @endif
    </div>

    <div class="form-group">
        <label for="source">Source</label>
        <input type="text" name="source" id="source" class="form-control" value="{{ old('source') }}" @if ($vuejs) :class="{'is-invalid': errors && errors.source}" v-model="source" @endif>
        @if ($vuejs)
            <div class="invalid-feedback" v-if="errors && errors.source" v-text="errors.source[0]"></div>
        @endif
    </div>

    <div class="form-group">
        <label for="body">Summary</label>
        <textarea name="body" id="body" class="form-control" @if ($vuejs) :class="{'is-invalid': errors && errors.body}" v-model="body" @endif>{{ old('body') }}</textarea>
        @if ($vuejs)
            <div class="invalid-feedback" v-if="errors && errors.body" v-text="errors.body[0]"></div>
        @endif
    </div>
</fieldset>

<div class="form-group">
    <div class="form-check">
        <input type="hidden" name="published" value="0" @if ($vuejs) v-model="published" @endif>
        <input class="form-check-input" type="checkbox" id="item_published" name="published" value="1" checked @if ($vuejs) v-model="published" @endif>
        <label class="form-check-label" for="item_published">@lang('interface.published')</label>
    </div>
</div>