@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>@lang('interface.admin.item.edit')</h1>

            <form action="{{ route('admin.items.update', $item) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('patch')

                @include('partials._errors')

                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" id="title" class="form-control" value="{{ $item->title }}">
                </div>

                <div class="form-group">
                    <label for="item_type_id">@lang('interface.item_type')</label>
                    <select name="item_type_id" id="item_type_id" class="form-control">
                        <option>-- @lang('interface.choose_option') --</option>
                        @foreach($itemTypes as $type)
                            <option value="{{ $type->id }}" @if ($type->id == $item->item_type_id) selected @endif>{{ $type->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    @if ($item->campaign)
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" disabled checked>
                            <label class="form-check-label">@lang('interface.campaign'): <a href="{{ route('admin.campaigns.edit', $item->campaign) }}">{{ $item->campaign->title }}</a></label>
                        </div>
                    @else
                        <p class="empty">@lang('interface.campaign'): @lang('interface.no_asigned_campaign')</p>
                    @endif
                </div>

                <fieldset class="form-group">
                    <legend>{{ $itemTypes[\App\ItemType::TYPE_BANNER-1]->name }}</legend>

                    @if ($currentImageUrl)
                        <div class="featured-image-thumb">
                            <figure class="image">
                                <img src="{{ $currentImageUrl }}" alt="">
                            </figure>
                            <label class="checkbox delete-image">
                                <input type="checkbox" name="delete_featured_image" value="1">
                                <span>@lang('interface.delete')</span>
                            </label>
                        </div>
                    @endif
                    <div class="form-group">
                        <input type="file" class="form-control-file" id="featured_image" name="featured_image">
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <legend>{{ $itemTypes[\App\ItemType::TYPE_VIDEO-1]->name }}</legend>

                    <div class="form-group">
                        <label for="video_url">URL Video</label>
                        <input type="text" name="video_url" id="video_url" size="50" class="form-control" value="{{ $item->video_url }}">
                        <small class="form-text text-muted">@lang('interface.admin.item.video_url.help')</small>
                    </div>
                </fieldset>

                <fieldset class="form-group">
                    <legend>{{ $itemTypes[\App\ItemType::TYPE_NEWS-1]->name }}</legend>

                    <div class="form-group">
                        <label for="url">Url</label>
                        <input type="text" name="url" id="url" class="form-control" value="{{ $item->url }}">
                    </div>

                    <div class="form-group">
                        <label for="source">Source</label>
                        <input type="text" name="source" id="source" class="form-control" value="{{ $item->source }}">
                    </div>

                    <div class="form-group">
                        <label for="body">Summary</label>
                        <textarea name="body" id="body" class="form-control">{{ $item->body }}</textarea>
                    </div>
                </fieldset>

                <div class="form-group">
                    <div class="form-check">
                        <input type="hidden" name="published" value="0">
                        <input class="form-check-input" type="checkbox" id="published" name="published" value="1"@if ($item->published) checked @endif>
                        <label class="form-check-label" for="published">@lang('interface.published')</label>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">@lang('interface.update')</button>
                    @can('delete', $item)
                    <a href="{{ route('admin.items.delete', $item) }}" class="btn btn-danger">@lang('interface.delete')</a>
                    @endcan
                    <a href="{{ route('admin.items.index') }}" class="btn btn-link">@lang('interface.cancel')</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
