@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>@lang('interface.admin.item.index')</h1>

                @can('create', \App\Item::class)
                    <a href="{{ route('admin.items.create') }}" class="btn btn-primary">@lang('interface.admin.item.create')</a>
                @endcan

                <hr>

                @if (count($items) > 0)
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Thumb</th>
                            <th scope="col">Title</th>
                            <th scope="col">Type</th>
                            <th scope="col">Published</th>
                            <th scope="col">Author</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $item)
                        <tr>
                            <th scope="row">{{ $item->id }}</th>
                            <td>
                                @if ($item->isBanner)
                                    <img src="{{ $item->getFirstMediaUrl('featured', 'item-teaser') }}" alt="">
                                @elseif ($item->isVideo)
                                    <img src="{{ $item->getFirstMediaUrl('youtube') }}" alt="">
                                @endif
                            </td>
                            <td>{{ str_limit($item->title, 60) }}</td>
                            <td>{{ $item->itemType->name }}</td>
                            <td>@if ($item->published) <i class="fas fa-check"></i> @else <i class="fas fa-times"></i> @endif</td>
                            <td>{{ $item->user->name }}</td>
                            <td>
                                @can('update', $item)
                                    <a href="{{ route('admin.items.edit', $item) }}" class="btn btn-primary btn-sm">@lang('interface.edit')</a>
                                @endcan
                                @can('delete', $item)
                                    <a href="{{ route('admin.items.delete', $item) }}" class="btn btn-primary btn-sm btn-danger">@lang('interface.delete')</a>
                                @endcan
                            </td>
                        </tr>
                        @endforeach

                        {{ $items->links() }}
                    </tbody>
                </table>
                @else
                    <p class="is-empty">@lang('interface.no_items')</p>
                @endif
            </div>
        </div>
    </div>
@endsection
