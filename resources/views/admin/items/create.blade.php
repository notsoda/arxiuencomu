@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>@lang('interface.admin.item.create')</h1>

            <form action="{{ route('admin.items.store') }}" method="post" enctype="multipart/form-data">
                @include('admin.items._createformfields', ['vuejs' => false])

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">@lang('interface.create')</button>
                    <a href="{{ route('admin.items.index') }}" class="btn btn-link">@lang('interface.cancel')</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
