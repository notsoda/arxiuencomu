@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>@lang('interface.admin.user.index')</h1>

                @can('create', \App\User::class)
                    <a href="{{ route('admin.users.create') }}" class="btn btn-primary">@lang('interface.admin.user.create')</a>
                @endcan

                <hr>

                @if (count($users) > 0)
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Role</th>
                            <th scope="col">E-mail</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <th scope="row">{{ $user->id }}</th>
                            <td>{{ $user->name }}</td>
                            @if ($user->role)
                                <td>{{ $user->role->name }}</td>
                            @else
                                <td>@lang('auth.role_anonymous')</td>
                            @endif
                            <td>{{ $user->email }}</td>
                            <td>
                                @can('update', $user)
                                    <a href="{{ route('admin.users.edit', $user) }}" class="btn btn-primary btn-sm">@lang('interface.edit')</a>
                                @endcan
                                @can('delete', $user)
                                    <a href="{{ route('admin.users.delete', $user) }}" class="btn btn-primary btn-sm btn-danger">@lang('interface.delete')</a>
                                @endcan
                            </td>
                        </tr>
                        @endforeach

                        {{ $users->links() }}
                    </tbody>
                </table>
                @else
                    <p class="is-empty">@lang('interface.no_users')</p>
                @endif
            </div>
        </div>
    </div>
@endsection
