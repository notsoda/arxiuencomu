@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>@lang('interface.admin.user.create')</h1>

            <form method="post" action="{{ route('admin.users.store') }}">
                @include('auth.registerfields')
            </form>
        </div>
    </div>
</div>
@endsection
