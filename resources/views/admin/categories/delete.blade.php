@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <p>@lang('interface.admin.sure_to_delete', ['title' => $category->name])</p>

            <form action="{{ route('admin.categories.destroy', $category) }}" method="post">
                @csrf
                @method('delete')

                <div class="form-group">
                    <button type="submit" class="btn btn-danger">@lang('interface.delete')</button>
                    <a href="{{ route('admin.categories.index') }}" class="btn btn-link">@lang('interface.cancel')</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
