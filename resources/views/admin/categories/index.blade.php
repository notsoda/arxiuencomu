@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>@lang('interface.admin.category.index')</h1>

                <hr>

                @can('create', \App\Category::class)
                    @include('admin.categories._create')
                    <hr>
                @endcan

                @if (count($categories) > 0)
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $category)
                        <tr>
                            <th scope="row">{{ $category->id }}</th>
                            <td>{{ $category->name }}</td>
                            <td>
                                @can('update', $category)
                                    <a href="{{ route('admin.categories.edit', $category) }}" class="btn btn-primary btn-sm">@lang('interface.edit')</a>
                                @endcan
                                @can('delete', $category)
                                    <a href="{{ route('admin.categories.delete', $category) }}" class="btn btn-primary btn-sm btn-danger">@lang('interface.delete')</a>
                                @endcan
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                    <p class="is-empty">@lang('interface.no_categories')</p>
                @endif
            </div>
        </div>
    </div>
@endsection
