<form action="{{ route('admin.tags.store') }}" method="post">
    @csrf

    @include('partials._errors')

    <div class="form-row">
        <div class="col-auto">
            <label for="name" class="sr-only">Name</label>
            <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
        </div>

        <div class="col-auto">
            <button type="submit" class="btn btn-primary">@lang('interface.create')</button>
        </div>
    </div>
</form>