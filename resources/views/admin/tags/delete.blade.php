@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <p>@lang('interface.admin.sure_to_delete', ['title' => $tag->name])</p>

            <form action="{{ route('admin.tags.destroy', $tag) }}" method="post">
                @csrf
                @method('delete')

                <div class="form-group">
                    <button type="submit" class="btn btn-danger">@lang('interface.delete')</button>
                    <a href="{{ route('admin.tags.index') }}" class="btn btn-link">@lang('interface.cancel')</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
