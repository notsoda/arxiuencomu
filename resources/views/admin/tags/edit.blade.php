@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>@lang('interface.admin.tag.edit')</h1>

                <form action="{{ route('admin.tags.update', $tag) }}" method="post">
                    @csrf
                    @method('patch')

                    @include('partials._errors')

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ $tag->name }}">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">@lang('interface.update')</button>
                        @can('delete', $tag)
                        <a href="{{ route('admin.tags.delete', $tag) }}" class="btn btn-danger">@lang('interface.delete')</a>
                        @endcan
                        <a href="{{ route('admin.tags.index') }}" class="btn btn-link">@lang('interface.cancel')</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection