@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>@lang('interface.admin.tag.index')</h1>

                <hr>

                @can('create', \App\Tag::class)
                    @include('admin.tags._create')
                    <hr>
                @endcan

                @if (count($tags) > 0)
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tags as $tag)
                        <tr>
                            <th scope="row">{{ $tag->id }}</th>
                            <td>{{ $tag->name }}</td>
                            <td>
                                @can('update', $tag)
                                    <a href="{{ route('admin.tags.edit', $tag) }}" class="btn btn-primary btn-sm">@lang('interface.edit')</a>
                                @endcan
                                @can('delete', $tag)
                                    <a href="{{ route('admin.tags.delete', $tag) }}" class="btn btn-primary btn-sm btn-danger">@lang('interface.delete')</a>
                                @endcan
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                    <p class="is-empty">@lang('interface.no_tags')</p>
                @endif
            </div>
        </div>
    </div>
@endsection
