<div class="col-6">
    <div class="card">
        <h5 class="card-header">@lang('interface.last_campaigns')</h5>

        @if (count($lastCampaigns) > 0)
            <ul class="list-group list-group-flush">
                @foreach($lastCampaigns as $campaign)
                    <li class="list-group-item">
                        {{ $campaign->title }}
                        @can('update', $campaign)
                            <a href="{{ route('admin.campaigns.edit', $campaign) }}" class="btn btn-primary btn-sm">Edit</a>
                        @endcan
                    </li>
                @endforeach
            </ul>
        @else
            <p class="is-empty">@lang('interface.no_campaigns')</p>
        @endif

        <div class="card-footer">
            <a href="{{ route('admin.campaigns.index') }}" class="btn btn-secondary">@lang('interface.manage_campaigns')</a>
            @can('create', \App\Campaign::class)
                <a href="{{ route('admin.campaigns.create') }}" class="btn btn-primary">@lang('interface.admin.campaign.create')</a>
            @endcan
        </div>
    </div>
</div>