<nav class="navbar navbar-expand-md navbar-dark bg-dark navbar-admin">
    <div class="container">
        <a class="navbar-brand" href="{{ route('dashboard') }}">
            @lang('interface.admin.dashboard.title')
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        @lang('interface.admin.dashboard.content') <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="{{ route('admin.campaigns.index') }}" class="dropdown-item">@lang('interface.admin.campaign.index')</a>
                        <a href="{{ route('admin.items.index') }}" class="dropdown-item">@lang('interface.admin.item.index')</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        @lang('interface.admin.dashboard.taxonomies') <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="{{ route('admin.categories.index') }}" class="dropdown-item">@lang('interface.admin.category.index')</a>
                        <a href="{{ route('admin.tags.index') }}" class="dropdown-item">@lang('interface.admin.tag.index')</a>
                    </div>
                </li>

                <li class="nav-item"><a href="{{ route('admin.users.index') }}" class="nav-link">@lang('interface.admin.user.index')</a></li>
            </ul>

            @include('admin.partials._authmenu')
        </div>
    </div>
</nav>