<div class="col-6">
    <div class="card">
        <h5 class="card-header">@lang('interface.last_items')</h5>

        @if (count($lastItems) > 0)
            <ul class="list-group list-group-flush">
                @foreach($lastItems as $item)
                    <li class="list-group-item">
                        <img src="{{ $item->getFirstMediaUrl('featured', 'item-teaser') }}" alt="">
                        {{ $item->title }}
                        @can('update', $item)
                            <a href="{{ route('admin.items.edit', $item) }}" class="btn btn-primary btn-sm">Edit</a>
                        @endcan
                    </li>
                @endforeach
            </ul>
        @else
            <p class="is-empty">@lang('interface.no_items')</p>
        @endif

        <div class="card-footer">
            <a href="{{ route('admin.items.index') }}" class="btn btn-secondary">@lang('interface.manage_items')</a>
            @can('create', \App\Item::class)
                <a href="{{ route('admin.items.create') }}" class="btn btn-primary">@lang('interface.admin.item.create')</a>
            @endcan
        </div>
    </div>
</div>