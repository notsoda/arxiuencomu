@section('scripts')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey={{ config('app.tinymce_app_key') }}"></script>
    <script>
        var editor_config = {
            selector: 'textarea',
            menubar: false,
            plugins : 'lists link image',
            toolbar: 'undo redo | styleselect | bold italic | bullist numlist outdent indent | link image',
            path_absolute : '/',
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'filemanager?field_name=' + field_name;
                if (type === 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }
                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };
        tinymce.init(editor_config);
    </script>
@endsection