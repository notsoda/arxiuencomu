<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Dashboard :: {{ config('app.name') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link href="{{ asset('css/admin/app.css') }}" rel="stylesheet">

    @yield('head')
</head>
<body>
    <div id="app" class="dashboard">
        @include('admin.partials._nav')

        <main class="py-4">
            @yield('content')
        </main>

        @include('admin.partials._flash')
    </div>

    <script src="{{ asset('js/admin/app.js') }}" defer></script>
    @yield('scripts')
</body>
</html>
