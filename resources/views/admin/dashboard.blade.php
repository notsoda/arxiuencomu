@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h1>@lang('interface.admin.dashboard.title')</h1>

                <div class="row">
                    @include('admin.partials._lastcampaigns')

                    @include('admin.partials._lastitems')
                </div>
            </div>
        </div>
    </div>
@endsection
