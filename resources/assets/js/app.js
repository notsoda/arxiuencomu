
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('search-form', require('./components/SearchForm'));
Vue.component('filters-form', require('./components/FiltersForm'));

const app = new Vue({
    el: '#app'
});

// Responsive videos
fitvids();

// collapse campaign details item lists
$('article.detail.campaign .items-list').each(function(i){
    const $itemsList = $(this);
    const countItems = $itemsList.find('.item-teaser').length;
    if (countItems > 4) {
        const allMessage = 'Veure tot ('+countItems+')';
        const lessMessage = 'Veure menys';
        const collapsedClass = 'collapsed';
        $itemsList.addClass(collapsedClass);
        $itemsList.after('<button data-action="view-all">'+allMessage+'</button>');
        $itemsList.parent().find('button[data-action="view-all"]').bind('click', function(e) {
            e.preventDefault();
            $itemsList.toggleClass(collapsedClass);
            if ($itemsList.hasClass(collapsedClass)) $(this).text(allMessage);
            else $(this).text(lessMessage);
        });
    }
});

// item detail open image zoom as overlay
$('article.detail.item figure.media a.view-full-image').colorbox({
    maxWidth: '95%',
    maxHeight: '95%'
});

// share links
const $shareLinks = $('.share-links');
if ($shareLinks.length) {
    $shareLinks.on('click', 'a', function(e){
        e.preventDefault();
        popupCenter($(this).attr('href'), $(this).find('.text').html(), [580, 470]);
    });
}

function popupCenter(url, title, measures) {
    var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop !== undefined ? window.screenTop : screen.top;
    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
    var left = ((width / 2) - (measures[0] / 2)) + dualScreenLeft;
    var top = ((height / 3) - (measures[1] / 3)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + measures[0] + ', height=' + measures[1] + ', top=' + top + ', left=' + left);
    if (window.focus) {
        newWindow.focus();
    }
}