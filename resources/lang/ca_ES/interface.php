<?php

return [
    
    /*
    |--------------------------------------------------------------------------
    | Interface Language Lines
    |--------------------------------------------------------------------------
    |
    | Various lines used in the interface.
    |
    */
    
    'admin' => [
        'sure_to_delete' => "Estàs segura que vols eliminar ':title'?",
        'dashboard' => [
            'title' => "Panell de control",
            'content' => "Contingut",
            'taxonomies' => "Taxonomies",
        ],
        'user' => [
            'index' => "Usuaris",
            'create' => "Crea un usuari",
            'created' => "L'usuari ha estat creat.",
            'edit' => "Edita un usuari",
            'updated' => "L'usuari ha estat editat.",
            'deleted' => "L'usuari ha estat eliminat.",
            'cannot_delete_self' => "No pots eliminar el teu propi usuari.",
        ],
        'campaign' => [
            'index' => "Campanyes",
            'create' => "Crea una campanya",
            'created' => "La campanya ha estat creada.",
            'edit' => "Edita una campanya",
            'updated' => "La campanya ha estat editada.",
            'deleted' => "La campanya ha estat eliminada.",
        ],
        'category' => [
            'index' => "Categories",
            'create' => "Crea una categoria",
            'created' => "La categoria ha estat creada.",
            'edit' => "Edita una categoria",
            'updated' => "La categoria ha estat editada.",
            'deleted' => "La categoria ha estat eliminada.",
        ],
        'tag' => [
            'index' => "Etiquetes",
            'create' => "Crea una etiqueta",
            'created' => "L'etiqueta ha estat creada.",
            'edit' => "Edita una etiqueta",
            'updated' => "L'etiqueta ha estat editada.",
            'deleted' => "L'etiqueta ha estat eliminada.",
        ],
        'item' => [
            'index' => "Items de campanya",
            'create' => "Crea un item de campanya",
            'created' => "L'item de campanya ha estat creat.",
            'edit' => "Edita un item de campanya",
            'updated' => "L'item de campanya ha estat editat.",
            'deleted' => "L'item de campanya ha estat eliminat.",
            'video_url' => [
                'help' => "La url del video de Youtube, tipus https://www.youtube.com/watch?v=S4gU2Tsv6hY",
            ]
        ],
    ],
    
    'page_not_found' => "Pàgina no trobada",
    'published' => "Publicat",
    'choose_option' => "Tria una opció",
    'view' => "Veure",
    'edit' => "Edita",
    'update' => "Actualitza",
    'create' => "Crea",
    'delete' => "Elimina",
    'cancel' => "Cancel·la",
    'no_users' => "Encara no s'ha creat cap usuari.",
    'category' => "Categoria/es",
    'district' => "Districte/s",
    'tag' => "Etiquetes",
    'campaign_date' => "Data",
    'manage_campaigns' => "Gestiona les campanyes",
    'manage_items' => "Gestiona els items de campanya",
    'campaign' => "Campanya",
    'no_campaigns' => "Encara no s'ha creat cap campanya.",
    'last_campaigns' => "Darreres campanyes",
    'last_items' => "Darrers items de campanya",
    'no_categories' => "Encara no s'ha creat cap categoria.",
    'no_tags' => "Encara no s'ha creat cap etiqueta.",
    'no_items' => "Encara no s'ha creat cap item de campanya.",
    'no_unasigned_items' => "No hi ha cap item no assignat a una campanya.",
    'no_asigned_campaign' => "No s'ha relacionat amb cap campanya.",
    'campaign_without_items' => "Aquesta campanya està buida de moment.",
    'item_type' => "Tipus",
    'image' => "Imatge",
    'share' => "Comparteix",
    'go_back' => "Torna",
    'download' => "Descarrega",
    'video_url_wrong' => "La url del video sembla incorrecta.",
    
    'search' => [
        'title' => "Cerca",
        'clear' => "Neteja la cerca",
        'placeholder' => "Cerca per paraules o etiquetes",
        'empty' => "No s'ha trobat cap campanya amb aquests criteris de cerca.",
    ],
    
    'filters' => [
        'filter_by' => "Filtra per",
        'category' => "categoria",
        'content' => "contingut",
        'district' => "districte",
    ],
];